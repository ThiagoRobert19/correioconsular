package com.trsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;


@Component("CarteiraEntity")
@Entity
public class CarteiraEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String protocol;

	@OneToOne
	@JoinColumn(name = "documentStatus_id")
	private DocumentStatusEntity documentStatus;// Recebido, PendÍncia,
												// Devolvido, Concedido

	private String personName;
	private String personPhone;
	private String personEmail;
	private String personAddress;


	private String trackingNumberEnvio;
	private String trackingNumberRetorno;

	@OneToOne
	@JoinColumn(name = "atendente_id")
	private UserEntity atendente;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateEntry;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateDeparture;
	
	
	
	
	
	public Integer getId() {
		return id;
	}





	public void setId(Integer id) {
		this.id = id;
	}





	public String getProtocol() {
		return protocol;
	}





	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}





	public DocumentStatusEntity getDocumentStatus() {
		return documentStatus;
	}





	public void setDocumentStatus(DocumentStatusEntity documentStatus) {
		this.documentStatus = documentStatus;
	}





	public String getPersonName() {
		return personName.toUpperCase();
	}





	public void setPersonName(String personName) {
		this.personName = personName.toUpperCase();
	}





	public String getPersonPhone() {
		return personPhone;
	}





	public void setPersonPhone(String personPhone) {
		this.personPhone = personPhone;
	}





	public String getPersonEmail() {
		return personEmail.toUpperCase();
	}





	public void setPersonEmail(String personEmail) {
		this.personEmail = personEmail.toUpperCase();
	}





	public String getPersonAddress() {
		return personAddress.toUpperCase();
	}





	public void setPersonAddress(String personAddress) {
		this.personAddress = personAddress.toUpperCase();
	}





	public String getTrackingNumberEnvio() {
		return trackingNumberEnvio.toUpperCase();
	}





	public void setTrackingNumberEnvio(String trackingNumberEnvio) {
		this.trackingNumberEnvio = trackingNumberEnvio.toUpperCase();
	}





	public String getTrackingNumberRetorno() {
		return trackingNumberRetorno.toUpperCase();
	}





	public void setTrackingNumberRetorno(String trackingNumberRetorno) {
		this.trackingNumberRetorno = trackingNumberRetorno.toUpperCase();
	}





	public UserEntity getAtendente() {
		return atendente;
	}





	public void setAtendente(UserEntity atendente) {
		this.atendente = atendente;
	}





	public Date getDateEntry() {
		return dateEntry;
	}





	public void setDateEntry(Date dateEntry) {
		this.dateEntry = dateEntry;
	}





	public Date getDateDeparture() {
		return dateDeparture;
	}





	public void setDateDeparture(Date dateDeparture) {
		this.dateDeparture = dateDeparture;
	}





	public static long getSerialversionuid() {
		return serialVersionUID;
	}





	@Override
	public String toString() {
		return "CarteiraEntity [id=" + id + ", protocol=" + protocol + ", documentStatus=" + documentStatus
				+ ", personName=" + personName + ", personPhone=" + personPhone + ", personEmail=" + personEmail
				+ ", personAddress=" + personAddress + ", trackingNumberEnvio=" + trackingNumberEnvio
				+ ", trackingNumberRetorno=" + trackingNumberRetorno + ", atendente=" + atendente + ", dateEntry="
				+ dateEntry + ", dateDeparture=" + dateDeparture + "]";
	}

	
	



}
