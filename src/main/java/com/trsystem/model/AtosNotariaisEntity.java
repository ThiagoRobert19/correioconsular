package com.trsystem.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;


@Component("AtosNotariaisEntity")
@Entity
public class AtosNotariaisEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String personName;
	private String personCity;
	
	@OneToOne
	@JoinColumn(name = "documentAtos_id")
	private DocumentAtosEntity documentAtos;
	
	@OneToOne
	@JoinColumn(name = "envelopeEntity_id")
	private EnvelopeEntity envelopeEntity;
	
	private Integer quantidade;
	@OneToOne
	@JoinColumn(name = "atendente_id")
	private UserEntity atendente;

	private String trackingNumberEnvio;
	private String trackingNumberRetorno;

	


	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateEntry;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateDeparture;

	@OneToOne
	@JoinColumn(name = "documentStatus_id")
	private DocumentStatusEntity documentStatus;
	
	private String observation;
	
	

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPersonCity() {
		return personCity;
	}

	public void setPersonCity(String personCity) {
		this.personCity = personCity;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getTrackingNumberEnvio() {
		return trackingNumberEnvio;
	}

	public void setTrackingNumberEnvio(String trackingNumberEnvio) {
		this.trackingNumberEnvio = trackingNumberEnvio;
	}

	public String getTrackingNumberRetorno() {
		return trackingNumberRetorno;
	}

	public void setTrackingNumberRetorno(String trackingNumberRetorno) {
		this.trackingNumberRetorno = trackingNumberRetorno;
	}

	public UserEntity getAtendente() {
		return atendente;
	}

	public void setAtendente(UserEntity atendente) {
		this.atendente = atendente;
	}

	public Date getDateEntry() {
		return dateEntry;
	}

	public void setDateEntry(Date dateEntry) {
		this.dateEntry = dateEntry;
	}

	public Date getDateDeparture() {
		return dateDeparture;
	}

	public void setDateDeparture(Date dateDeparture) {
		this.dateDeparture = dateDeparture;
	}

	public DocumentStatusEntity getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatusEntity documentStatus) {
		this.documentStatus = documentStatus;
	}

	public DocumentAtosEntity getDocumentAtos() {
		return documentAtos;
	}

	public void setDocumentAtos(DocumentAtosEntity documentAtos) {
		this.documentAtos = documentAtos;
	}

	public EnvelopeEntity getEnvelopeEntity() {
		return envelopeEntity;
	}

	public void setEnvelopeEntity(EnvelopeEntity envelopeEntity) {
		this.envelopeEntity = envelopeEntity;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AtosNotariaisEntity [id=" + id + ", personName=" + personName + ", personCity=" + personCity
				+ ", quantidade=" + quantidade + ", trackingNumberEnvio=" + trackingNumberEnvio
				+ ", trackingNumberRetorno=" + trackingNumberRetorno + ", atendente=" + atendente + ", dateEntry="
				+ dateEntry + ", dateDeparture=" + dateDeparture + ", documentStatus=" + documentStatus
				+ ", documentAtos=" + documentAtos + ", envelopeEntity=" + envelopeEntity + "]";
	}

	



}
