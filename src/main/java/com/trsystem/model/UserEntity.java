package com.trsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.trsystem.util.Criptografia;
import com.trsystem.util.EntidadeBase;

@Component("UserEntity")
@Entity
public class UserEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String email;

	private String acessoCmc;
	private String acessoPassaporte;
	private String acessoAtos;
	private String tipoAdm;// Passaporte, Atos, Todos,Nao
	
	
	private String tipo;// Administrador,Atendente
	private String cargo;// Auxiliar Administrativo, Auxiliar De
							// Apoio, Assistente Tecnico, Vice Consul
	private String tipoCargo;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date nascimento;
	private String sexo;
	private String nit;
	private String nacionalidade;
	private String naturalidade;
	private String estadoCivil;
	private String statusImigratorio;

	private String address;
	private String city;
	private String state;
	private String zipcode;
	private String phone;
	private String cellphone;

	private String emailParticular;
	private String militar;

	private String contatoEmergencia;
	private String nomeContatoEmergencia;
	private String relacaoContatoEmergencia;

	private String status;// able, disable

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date admissionDate;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date resignationDate;

	private String motherName;
	private String fatherName;
	private String cpf;
	private String rg;
	private String tituloEleitoral;
	private String ssn;
	private String passaporte;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date validadePass;

	private String cnh;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date validadeCnh;

	private String formacaoAcademica;
	private String tituloAcademico;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date conclusaoAcademica;

	private String instituicaoAcademica;

	private boolean login;
	private String password;

	
	@OneToOne
	@JoinColumn(name = "secao_id")
	private SecaoEntity secao;
	


	public String getAcessoCmc() {
		return acessoCmc;
	}

	public void setAcessoCmc(String acessoCmc) {
		this.acessoCmc = acessoCmc;
	}

	
	public SecaoEntity getSecao() {
		return secao;
	}

	public void setSecao(SecaoEntity secao) {
		this.secao = secao;
	}

	public String getAcessoPassaporte() {
		return acessoPassaporte;
	}

	public void setAcessoPassaporte(String acessoPassaporte) {
		this.acessoPassaporte = acessoPassaporte;
	}

	public String getAcessoAtos() {
		return acessoAtos;
	}

	public void setAcessoAtos(String acessoAtos) {
		this.acessoAtos = acessoAtos;
	}

	public String getTipoAdm() {
		return tipoAdm;
	}

	public void setTipoAdm(String tipoAdm) {
		this.tipoAdm = tipoAdm;
	}

	public String getTipoCargo() {
		return tipoCargo;
	}

	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getNit() {
		return nit;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getStatusImigratorio() {
		return statusImigratorio;
	}

	public void setStatusImigratorio(String statusImigratorio) {
		this.statusImigratorio = statusImigratorio;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getEmailParticular() {
		return emailParticular;
	}

	public void setEmailParticular(String emailParticular) {
		this.emailParticular = emailParticular;
	}

	public String getMilitar() {
		return militar;
	}

	public void setMilitar(String militar) {
		this.militar = militar;
	}

	public String getContatoEmergencia() {
		return contatoEmergencia;
	}

	public void setContatoEmergencia(String contatoEmergencia) {
		this.contatoEmergencia = contatoEmergencia;
	}

	public String getNomeContatoEmergencia() {
		return nomeContatoEmergencia;
	}

	public void setNomeContatoEmergencia(String nomeContatoEmergencia) {
		this.nomeContatoEmergencia = nomeContatoEmergencia;
	}

	public String getRelacaoContatoEmergencia() {
		return relacaoContatoEmergencia;
	}

	public void setRelacaoContatoEmergencia(String relacaoContatoEmergencia) {
		this.relacaoContatoEmergencia = relacaoContatoEmergencia;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getTituloEleitoral() {
		return tituloEleitoral;
	}

	public void setTituloEleitoral(String tituloEleitoral) {
		this.tituloEleitoral = tituloEleitoral;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}

	public Date getValidadePass() {
		return validadePass;
	}

	public void setValidadePass(Date validadePass) {
		this.validadePass = validadePass;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public Date getValidadeCnh() {
		return validadeCnh;
	}

	public void setValidadeCnh(Date validadeCnh) {
		this.validadeCnh = validadeCnh;
	}

	public String getFormacaoAcademica() {
		return formacaoAcademica;
	}

	public void setFormacaoAcademica(String formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}

	public String getTituloAcademico() {
		return tituloAcademico;
	}

	public void setTituloAcademico(String tituloAcademico) {
		this.tituloAcademico = tituloAcademico;
	}

	public Date getConclusaoAcademica() {
		return conclusaoAcademica;
	}

	public void setConclusaoAcademica(Date conclusaoAcademica) {
		this.conclusaoAcademica = conclusaoAcademica;
	}

	public String getInstituicaoAcademica() {
		return instituicaoAcademica;
	}

	public void setInstituicaoAcademica(String instituicaoAcademica) {
		this.instituicaoAcademica = instituicaoAcademica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isLogin() {
		return login;
	}

	public void setLogin(boolean login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getResignationDate() {
		return resignationDate;
	}

	public void setResignationDate(Date resignationDate) {
		this.resignationDate = resignationDate;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", name=" + name + ", email=" + email + ", acessoCmc=" + acessoCmc
				+ ", acessoPassaporte=" + acessoPassaporte + ", acessoAtos=" + acessoAtos + ", tipoAdm=" + tipoAdm
				+ ", tipo=" + tipo + ", cargo=" + cargo + ", tipoCargo=" + tipoCargo + ", nascimento=" + nascimento
				+ ", sexo=" + sexo + ", nit=" + nit + ", nacionalidade=" + nacionalidade + ", naturalidade="
				+ naturalidade + ", estadoCivil=" + estadoCivil + ", statusImigratorio=" + statusImigratorio
				+ ", address=" + address + ", city=" + city + ", state=" + state + ", zipcode=" + zipcode + ", phone="
				+ phone + ", cellphone=" + cellphone + ", emailParticular=" + emailParticular + ", militar=" + militar
				+ ", contatoEmergencia=" + contatoEmergencia + ", nomeContatoEmergencia=" + nomeContatoEmergencia
				+ ", relacaoContatoEmergencia=" + relacaoContatoEmergencia + ", status=" + status + ", admissionDate="
				+ admissionDate + ", resignationDate=" + resignationDate + ", motherName=" + motherName
				+ ", fatherName=" + fatherName + ", cpf=" + cpf + ", rg=" + rg + ", tituloEleitoral=" + tituloEleitoral
				+ ", ssn=" + ssn + ", passaporte=" + passaporte + ", validadePass=" + validadePass + ", cnh=" + cnh
				+ ", validadeCnh=" + validadeCnh + ", formacaoAcademica=" + formacaoAcademica + ", tituloAcademico="
				+ tituloAcademico + ", conclusaoAcademica=" + conclusaoAcademica + ", instituicaoAcademica="
				+ instituicaoAcademica + ", login=" + login + ", password=" + password + "]";
	}



}
