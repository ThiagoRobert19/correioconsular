package com.trsystem.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;

@Component("SetorEntity")
@Entity
public class SetorEntity  implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String name;
	private String detail;

	@OneToOne
	@JoinColumn(name = "chefe_id")
	private UserEntity chefe;
	@OneToOne
	@JoinColumn(name = "substituto_id")
	private UserEntity substituto;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public UserEntity getChefe() {
		return chefe;
	}
	public void setChefe(UserEntity chefe) {
		this.chefe = chefe;
	}
	public UserEntity getSubstituto() {
		return substituto;
	}
	public void setSubstituto(UserEntity substituto) {
		this.substituto = substituto;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "SetorEntity [id=" + id + ", name=" + name + ", detail=" + detail + ", chefe=" + chefe + ", substituto="
				+ substituto + "]";
	}
	
	
	
}
