package com.trsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;

@Component("AfastamentoEntity")
@Entity
public class AfastamentoEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateStart;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateEnd;

	
	private String startCalendar;
	private String endCalendar;
	private String title;// userEntity
	

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateRequest;

	@OneToOne
	@JoinColumn(name = "requester_id")
	private UserEntity requester;

	@OneToOne
	@JoinColumn(name = "chefeImediato_id")
	private UserEntity chefeImediato;

	@OneToOne
	@JoinColumn(name = "administrador_id")
	private UserEntity administrador;

	@OneToOne
	@JoinColumn(name = "chefeChancelaria_id")
	private UserEntity chefeChancelaria;

	@OneToOne
	@JoinColumn(name = "substitute_id")
	private UserEntity substitute;

	private String observacaoImediato;
	private String observacaoAdminsitracao;
	private String observacaoChefePosto;

	private String tipo;// Ferias, Sick Leave,Medical
						// Leave,Maternidade,Paternidade, Nojo, Juri
	private String uuid;

	private String status;// Aprovado, Negado, Pendente Chefe Imediato, Pendente Administracao, Aprovado Chefe Imediato, Aprovado Administracao
							

	private String motivoNegado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getStartCalendar() {
		return startCalendar;
	}

	public void setStartCalendar(String startCalendar) {
		this.startCalendar = startCalendar;
	}

	public String getEndCalendar() {
		return endCalendar;
	}

	public void setEndCalendar(String endCalendar) {
		this.endCalendar = endCalendar;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateRequest() {
		return dateRequest;
	}

	public void setDateRequest(Date dateRequest) {
		this.dateRequest = dateRequest;
	}

	public UserEntity getRequester() {
		return requester;
	}

	public void setRequester(UserEntity requester) {
		this.requester = requester;
	}

	public UserEntity getChefeImediato() {
		return chefeImediato;
	}

	public void setChefeImediato(UserEntity chefeImediato) {
		this.chefeImediato = chefeImediato;
	}

	public UserEntity getAdministrador() {
		return administrador;
	}

	public void setAdministrador(UserEntity administrador) {
		this.administrador = administrador;
	}

	public UserEntity getChefeChancelaria() {
		return chefeChancelaria;
	}

	public void setChefeChancelaria(UserEntity chefeChancelaria) {
		this.chefeChancelaria = chefeChancelaria;
	}

	public UserEntity getSubstitute() {
		return substitute;
	}

	public void setSubstitute(UserEntity substitute) {
		this.substitute = substitute;
	}

	public String getObservacaoImediato() {
		return observacaoImediato;
	}

	public void setObservacaoImediato(String observacaoImediato) {
		this.observacaoImediato = observacaoImediato;
	}

	public String getObservacaoAdminsitracao() {
		return observacaoAdminsitracao;
	}

	public void setObservacaoAdminsitracao(String observacaoAdminsitracao) {
		this.observacaoAdminsitracao = observacaoAdminsitracao;
	}

	public String getObservacaoChefePosto() {
		return observacaoChefePosto;
	}

	public void setObservacaoChefePosto(String observacaoChefePosto) {
		this.observacaoChefePosto = observacaoChefePosto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMotivoNegado() {
		return motivoNegado;
	}

	public void setMotivoNegado(String motivoNegado) {
		this.motivoNegado = motivoNegado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AfastamentoEntity [id=" + id + ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + ", startCalendar="
				+ startCalendar + ", endCalendar=" + endCalendar + ", title=" + title + ", dateRequest=" + dateRequest
				+ ", requester=" + requester + ", chefeImediato=" + chefeImediato + ", administrador=" + administrador
				+ ", chefeChancelaria=" + chefeChancelaria + ", substitute=" + substitute + ", observacaoImediato="
				+ observacaoImediato + ", observacaoAdminsitracao=" + observacaoAdminsitracao
				+ ", observacaoChefePosto=" + observacaoChefePosto + ", tipo=" + tipo + ", uuid=" + uuid + ", status="
				+ status + ", motivoNegado=" + motivoNegado + "]";
	}

	
}
