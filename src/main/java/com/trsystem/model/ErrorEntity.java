package com.trsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;

@Component("ErrorEntity")
@Entity
public class ErrorEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String uuid;
	private String detail;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dateEntry;
	@DateTimeFormat(pattern = "HH:mm")
	@Temporal(TemporalType.DATE)
	private Date hourEntry;
	
	@OneToOne
	@JoinColumn(name = "userEntity_id")
	private UserEntity userEntity;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getDateEntry() {
		return dateEntry;
	}

	public void setDateEntry(Date dateEntry) {
		this.dateEntry = dateEntry;
	}

	public Date getHourEntry() {
		return hourEntry;
	}

	public void setHourEntry(Date hourEntry) {
		this.hourEntry = hourEntry;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ErrorEntity [id=" + id + ", detail=" + detail + ", dateEntry=" + dateEntry + ", hourEntry=" + hourEntry
				+ ", userEntity=" + userEntity + "]";
	}

	

}
