package com.trsystem.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;

@Component("DocumentEntity")
@Entity
public class DocumentEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String localDocumento;
	private String nomeDocumento;
	@OneToOne
	@JoinColumn(name = "passaporteEntity_id")
	private PassaporteEntity passaporteEntity;
	@OneToOne
	@JoinColumn(name = "carteiraEntity_id")
	private CarteiraEntity carteiraEntity;
	
	@OneToOne
	@JoinColumn(name = "afastamentoEntity_id")
	private AfastamentoEntity afastamentoEntity;

	@OneToOne
	@JoinColumn(name = "userEntity_id")
	private UserEntity userEntity;

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public AfastamentoEntity getAfastamentoEntity() {
		return afastamentoEntity;
	}

	public void setAfastamentoEntity(AfastamentoEntity afastamentoEntity) {
		this.afastamentoEntity = afastamentoEntity;
	}

	public String getNomeDocumento() {
		return nomeDocumento;
	}

	public void setNomeDocumento(String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLocalDocumento() {
		return localDocumento;
	}

	public void setLocalDocumento(String localDocumento) {
		this.localDocumento = localDocumento;
	}

	public PassaporteEntity getPassaporteEntity() {
		return passaporteEntity;
	}

	public void setPassaporteEntity(PassaporteEntity passaporteEntity) {
		this.passaporteEntity = passaporteEntity;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CarteiraEntity getCarteiraEntity() {
		return carteiraEntity;
	}

	public void setCarteiraEntity(CarteiraEntity carteiraEntity) {
		this.carteiraEntity = carteiraEntity;
	}

	@Override
	public String toString() {
		return "DocumentEntity [id=" + id + ", localDocumento=" + localDocumento + ", nomeDocumento=" + nomeDocumento
				+ ", passaporteEntity=" + passaporteEntity + ", carteiraEntity=" + carteiraEntity
				+ ", afastamentoEntity=" + afastamentoEntity + ", userEntity=" + userEntity + "]";
	}

	
	

}
