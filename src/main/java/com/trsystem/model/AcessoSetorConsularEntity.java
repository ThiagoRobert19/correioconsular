package com.trsystem.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;

@Component("AcessoSetorConsularEntity")
@Entity
public class AcessoSetorConsularEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@OneToOne
	@JoinColumn(name = "userEntity_id")
	private UserEntity userEntity;

	private String tipo;// Atendente, Administrador

	private String cmc;
	private String passaporte;// Sim,Nao
	private String atosNotariais;// Sim,Nao
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserEntity getUserEntity() {
		return userEntity;
	}
	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getCmc() {
		return cmc;
	}
	public void setCmc(String cmc) {
		this.cmc = cmc;
	}
	public String getPassaporte() {
		return passaporte;
	}
	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}
	public String getAtosNotariais() {
		return atosNotariais;
	}
	public void setAtosNotariais(String atosNotariais) {
		this.atosNotariais = atosNotariais;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "AcessoSetorConsularEntity [id=" + id + ", userEntity=" + userEntity + ", tipo=" + tipo + ", passaporte="
				+ passaporte + ", atosNotariais=" + atosNotariais + "]";
	}
	
	
}
