package com.trsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.trsystem.util.EntidadeBase;

@Component("ObservationHistoryEntity")
@Entity
public class ObservationHistoryEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String tipoDocumento;
	private String detail;
	@OneToOne
	@JoinColumn(name = "passaporteEntity_id")
	private PassaporteEntity passaporteEntity;
	@OneToOne
	@JoinColumn(name = "carteiraEntity_id")
	private CarteiraEntity carteiraEntity;

	private String observation;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.DATE)
	private Date dateObervation;

	@OneToOne
	@JoinColumn(name = "userEntity_id")
	private UserEntity userEntity;

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTipoDocumento() {
		return tipoDocumento.toUpperCase();
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento.toUpperCase();
	}

	public PassaporteEntity getPassaporteEntity() {
		return passaporteEntity;
	}

	public void setPassaporteEntity(PassaporteEntity passaporteEntity) {
		this.passaporteEntity = passaporteEntity;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Date getDateObervation() {
		return dateObervation;
	}

	public void setDateObervation(Date dateObervation) {
		this.dateObervation = dateObervation;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CarteiraEntity getCarteiraEntity() {
		return carteiraEntity;
	}

	public void setCarteiraEntity(CarteiraEntity carteiraEntity) {
		this.carteiraEntity = carteiraEntity;
	}

	@Override
	public String toString() {
		return "ObservationHistoryEntity [id=" + id + ", tipoDocumento=" + tipoDocumento + ", detail=" + detail
				+ ", passaporteEntity=" + passaporteEntity + ", carteiraEntity=" + carteiraEntity + ", observation="
				+ observation + ", dateObervation=" + dateObervation + ", userEntity=" + userEntity + "]";
	}

	
}
