package com.trsystem.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AcessoSetorConsularEntity;
import com.trsystem.model.DocumentEntity;
import com.trsystem.model.DocumentStatusEntity;
import com.trsystem.model.ObservationHistoryEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;

@Controller
@RequestMapping(value = "/relatorio")
public class RelatorioController {

	@Autowired
	private GenericDao<PassaporteEntity> daoPassaporte;
	@Autowired
	private GenericDao<UserEntity> daoUser;
	@Autowired
	private List<UserEntity> listUser;
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private List<PassaporteEntity> list;
	@Autowired
	private PassaporteEntity passaporteEntity;

	@Autowired
	private GenericDao<DocumentStatusEntity> daoDocumentStatus;

	@Autowired
	private DocumentStatusEntity documentStatusEntity;
	@Autowired
	private List<DocumentStatusEntity> listStatus;

	@Autowired
	private List<ObservationHistoryEntity> listObservation;
	@Autowired
	private GenericDao<ObservationHistoryEntity> daoObservation;
	@Autowired
	private ObservationHistoryEntity observationEntity;

	@Autowired
	private DocumentEntity documentEntity;
	@Autowired
	private List<DocumentEntity> listDocument;
	@Autowired
	private GenericDao<DocumentEntity> daoDocument;
	@Autowired
	private List<AcessoSetorConsularEntity> listAcesso;
	@Autowired
	private GenericDao<AcessoSetorConsularEntity> daoAcesso;

	@RequestMapping(value = { "/passaporte" }, method = RequestMethod.GET)
	public ModelAndView add(HttpServletRequest request, ModelAndView model) {
		listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.status", "able");

		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map, "and");

		model.addObject("listStatus", listStatus);
		model.addObject("listAcesso", listAcesso);
		model.setViewName("relatorio/passaporte");

		return model;

	}

	@RequestMapping(value = "/passaporte/entradadeate", method = RequestMethod.POST)
	public ModelAndView deate(String de, String ate, ModelAndView model) {
		Date passaDe = Common.passaDate(de);
		Date passaAte = Common.passaDate(ate);

		Date hoje = new Date();
		hoje = Common.formata(hoje);

		list = daoPassaporte.listDateMaiorMenor(PassaporteEntity.class, "dateEntry", "dateEntry", passaDe.toString(),
				passaAte.toString());

		listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.status", "able");

		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map, "and");

		model.addObject("listStatus", listStatus);
		model.addObject("listAcesso", listAcesso);

		model.addObject("list", list);
		model.setViewName("relatorio/passaporte");
		return model;

	}
	@RequestMapping(value = "/passaporte/saidadeate", method = RequestMethod.POST)
	public ModelAndView saidadeate(String de, String ate, ModelAndView model) {
		Date passaDe = Common.passaDate(de);
		Date passaAte = Common.passaDate(ate);

		Date hoje = new Date();
		hoje = Common.formata(hoje);

		list = daoPassaporte.listDateMaiorMenor(PassaporteEntity.class, "dateDeparture", "dateDeparture", passaDe.toString(),
				passaAte.toString());

		listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.status", "able");

		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map, "and");

		model.addObject("listStatus", listStatus);
		model.addObject("listAcesso", listAcesso);

		model.addObject("list", list);
		model.setViewName("relatorio/passaporte");
		return model;

	}
	@RequestMapping(value = "/passaporte/atendente", method = RequestMethod.POST)
	public ModelAndView atendente(String userID, ModelAndView model) {
		
		Map<String, Object> mappass = new HashMap<String, Object>();
		mappass.put("atendente.id", Integer.parseInt(userID));
		
		list = daoPassaporte.listarProperty(PassaporteEntity.class, mappass, "and");
			

		listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.status", "able");

		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map, "and");

		model.addObject("listStatus", listStatus);
		model.addObject("listAcesso", listAcesso);

		model.addObject("list", list);
		model.setViewName("relatorio/passaporte");
		return model;

	}
	@RequestMapping(value = "/passaporte/status", method = RequestMethod.POST)
	public ModelAndView status(String statusID, ModelAndView model) {
		
		Map<String, Object> mappass = new HashMap<String, Object>();
		mappass.put("documentStatus.id", Integer.parseInt(statusID));
		
		list = daoPassaporte.listarProperty(PassaporteEntity.class, mappass, "and");
			

		listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.status", "able");

		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map, "and");

		model.addObject("listStatus", listStatus);
		model.addObject("listAcesso", listAcesso);

		model.addObject("list", list);
		model.setViewName("relatorio/passaporte");
		return model;

	}
}
