package com.trsystem.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AtosNotariaisEntity;
import com.trsystem.model.DocumentAtosEntity;
import com.trsystem.model.DocumentStatusEntity;
import com.trsystem.model.EnvelopeEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;

@Controller
@RequestMapping(value = "/atos")
public class AtosController {
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private GenericDao<AtosNotariaisEntity> daoAtos;
	@Autowired
	private List<AtosNotariaisEntity> list;
	@Autowired
	private AtosNotariaisEntity atosEntity;

	@Autowired
	private DocumentAtosEntity documentAtos;
	@Autowired
	private GenericDao<DocumentAtosEntity> daoDocuments;
	@Autowired
	private List<DocumentAtosEntity> listDocuments;
	@Autowired
	private EnvelopeEntity envelopeEntity;
	@Autowired
	private GenericDao<EnvelopeEntity> daoEnvelope;
	@Autowired
	private List<EnvelopeEntity> listEnvelopes;
	@Autowired
	private DocumentStatusEntity documentStatusEntity;
	@Autowired
	private GenericDao<DocumentStatusEntity> daoDocumentStatus;
	@Autowired
	private List<DocumentStatusEntity> listDocumentStatus;

	@RequestMapping(value = { "/data" }, method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, ModelAndView model) {

		list = daoAtos.list(AtosNotariaisEntity.class);

		model.addObject("list", list);

		model.setViewName("atos/data");
		return model;

	}

	@RequestMapping(value = { "/add" }, method = RequestMethod.GET)
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		listEnvelopes = daoEnvelope.list(EnvelopeEntity.class);
		listDocuments = daoDocuments.list(DocumentAtosEntity.class);
		listDocumentStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

		model.addObject("userEntity", userEntity);
		model.addObject("listEnvelopes", listEnvelopes);
		model.addObject("listDocuments", listDocuments);
		model.addObject("listDocumentStatus", listDocumentStatus);
		model.setViewName("atos/add");

		return model;

	}

	@RequestMapping(value = "/saveupdate", method = RequestMethod.POST)
	public ModelAndView saveupdate(AtosNotariaisEntity atos, String documentID, String envelopeID, String statusID,
			String dataentrada, String datasaida, HttpServletRequest request, ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		documentAtos = daoDocuments.buscaId(DocumentAtosEntity.class, Integer.parseInt(documentID));
		envelopeEntity = daoEnvelope.buscaId(EnvelopeEntity.class, Integer.parseInt(envelopeID));
		documentStatusEntity = daoDocumentStatus.buscaId(DocumentStatusEntity.class, Integer.parseInt(statusID));

		atos.setAtendente(userEntity);
		atos.setDocumentAtos(documentAtos);
		atos.setEnvelopeEntity(envelopeEntity);
		atos.setDocumentStatus(documentStatusEntity);

		if (dataentrada != null && !dataentrada.equals("")) {
			atos.setDateEntry(Common.passaDate(dataentrada));
		}

		if (datasaida != null && !datasaida.equals("")) {
			atos.setDateDeparture(Common.passaDate(datasaida));
		}
		daoAtos.saveUpdate(atos);

		model.setViewName("redirect:/atos/data");

		return model;

	}

	@RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoAtos.exist(AtosNotariaisEntity.class, map, "and")) {
			atosEntity = daoAtos.findByProperty(AtosNotariaisEntity.class, map, "and");

			listEnvelopes = daoEnvelope.list(EnvelopeEntity.class);
			listDocuments = daoDocuments.list(DocumentAtosEntity.class);
			listDocumentStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

			model.addObject("listEnvelopes", listEnvelopes);
			model.addObject("listDocuments", listDocuments);
			model.addObject("listDocumentStatus", listDocumentStatus);

			model.addObject("atosEntity", atosEntity);
			model.setViewName("/atos/edit");
			return model;
		} else {
			model.setViewName("redirect:/atos/data");
			return model;
		}

	}

	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoAtos.exist(AtosNotariaisEntity.class, map, "and")) {

			daoAtos.delete(AtosNotariaisEntity.class, map, "and");

			model.setViewName("redirect:/atos/data");
			return model;
		} else {
			model.setViewName("redirect:/atos/data");
			return model;
		}

	}
	@RequestMapping(value = { "/quando/{texto}" }, method = RequestMethod.GET)
	public ModelAndView inseridosdata(@PathVariable(value = "texto") String texto, HttpServletRequest request,
			ModelAndView model) {
		Calendar cal1 = Calendar.getInstance();
		int year = cal1.get(Calendar.YEAR);
		Date hoje = new Date();
		if (texto.equals("hoje")) {
			hoje = Common.formata(hoje);
			list = daoAtos.listDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry", hoje.toString(),
					hoje.toString());
		}
		if (texto.equals("semana")) {
			Calendar dateweek = Calendar.getInstance();

			dateweek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

			Date semanaInicio = Common.formata(dateweek.getTime());
			for (int i = 0; i < 6; i++) {
				dateweek.add(Calendar.DATE, 1);
			}

			Date semanaFim = Common.formata(dateweek.getTime());

			list = daoAtos.listDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry",
					semanaInicio.toString(), semanaFim.toString());

		}
		if (texto.equals("mes")) {

			int firstDate = cal1.getActualMinimum(Calendar.DATE);
			int month = cal1.get(Calendar.MONTH);

			int lastDate = cal1.getActualMaximum(Calendar.DATE);

			cal1.set(year, month, firstDate);

			Date mesInicio = Common.formata(cal1.getTime());

			cal1.set(year, month, lastDate);

			Date mesFim = Common.formata(cal1.getTime());

			list = daoAtos.listDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry",
					mesInicio.toString(), mesFim.toString());

		}
		if (texto.equals("ano")) {

			int diaum = 1;
			int monthJaneiro = 0;
			int monthdezembro = 11;
			int diaultimo = 31;

			cal1.set(year, monthJaneiro, diaum);

			Date primeiradata = Common.formata(cal1.getTime());

			cal1.set(year, monthdezembro, diaultimo);

			Date ultimadata = Common.formata(cal1.getTime());

			list = daoAtos.listDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry",
					primeiradata.toString(), ultimadata.toString());

		}
		if (!texto.equals("hoje") && texto.equals("semana") && texto.equals("mes") && texto.equals("ano")) {
			model.setViewName("redirect:/");

			return model;

		}

		model.addObject("textodata", texto);
		model.addObject("list", list);
		model.setViewName("atos/inseridosdata");

		return model;

	}

}
