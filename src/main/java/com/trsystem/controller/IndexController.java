package com.trsystem.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AtosNotariaisEntity;
import com.trsystem.model.CarteiraEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;

@Controller
public class IndexController {

	@Autowired
	private GenericDao<PassaporteEntity> daoPassaporte;

	@Autowired
	private GenericDao<AtosNotariaisEntity> daoAtos;
	@Autowired
	private GenericDao<CarteiraEntity> daoCarteira;

	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, ModelAndView model) {

		if ((UserEntity) request.getSession().getAttribute("clienteLogado") != null) {
			Calendar dateweek = Calendar.getInstance();

			dateweek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

			Date semanaInicio = Common.formata(dateweek.getTime());
			for (int i = 0; i < 6; i++) {
				dateweek.add(Calendar.DATE, 1);
			}

			Date semanaFim = Common.formata(dateweek.getTime());
			Calendar cal1 = Calendar.getInstance();
			int firstDate = cal1.getActualMinimum(Calendar.DATE);
			int month = cal1.get(Calendar.MONTH);
			int year = cal1.get(Calendar.YEAR);
			int lastDate = cal1.getActualMaximum(Calendar.DATE);

			cal1.set(year, month, firstDate);

			Date mesInicio = Common.formata(cal1.getTime());

			cal1.set(year, month, lastDate);

			Date mesFim = Common.formata(cal1.getTime());

			int diaum = 1;
			int monthJaneiro = 0;
			int monthdezembro = 11;
			int diaultimo = 31;

			cal1.set(year, monthJaneiro, diaum);

			Date primeiradata = Common.formata(cal1.getTime());

			cal1.set(year, monthdezembro, diaultimo);

			Date ultimadata = Common.formata(cal1.getTime());

			int countano = daoPassaporte.countDateMaiorMenor(PassaporteEntity.class, "dateEntry", "dateEntry",
					primeiradata.toString(), ultimadata.toString());
			int countmes = daoPassaporte.countDateMaiorMenor(PassaporteEntity.class, "dateEntry", "dateEntry",
					mesInicio.toString(), mesFim.toString());
			int countsemana = daoPassaporte.countDateMaiorMenor(PassaporteEntity.class, "dateEntry", "dateEntry",
					semanaInicio.toString(), semanaFim.toString());
			int counttotal = daoPassaporte.count(PassaporteEntity.class);

			int countatosmes = daoAtos.countDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry",
					mesInicio.toString(), mesFim.toString());
			int countatosano = daoAtos.countDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry",
					primeiradata.toString(), ultimadata.toString());
			int countatossemana = daoAtos.countDateMaiorMenor(AtosNotariaisEntity.class, "dateEntry", "dateEntry",
					semanaInicio.toString(), semanaFim.toString());
			int countatostotal = daoAtos.count(AtosNotariaisEntity.class);

			int countcmcmes = daoCarteira.countDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry",
					mesInicio.toString(), mesFim.toString());
			int countcmcano = daoCarteira.countDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry",
					primeiradata.toString(), ultimadata.toString());
			int countcmcsemana = daoCarteira.countDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry",
					semanaInicio.toString(), semanaFim.toString());
			int countcmctotal = daoCarteira.count(CarteiraEntity.class);

			model.addObject("counttotal", counttotal);
			model.addObject("countsemana", countsemana);
			model.addObject("countmes", countmes);
			model.addObject("countano", countano);

			model.addObject("countatostotal", countatostotal);
			model.addObject("countatosano", countatosano);
			model.addObject("countatosmes", countatosmes);
			model.addObject("countatossemana", countatossemana);

			model.addObject("countcmctotal", countcmctotal);
			model.addObject("countcmcano", countcmcano);
			model.addObject("countcmcmes", countcmcmes);
			model.addObject("countcmcsemana", countcmcsemana);

			model.setViewName("index/index");

			return model;
		} else {
			model.setViewName("index/index");

			return model;
		}

	}

}