package com.trsystem.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AfastamentoEntity;
import com.trsystem.model.DocumentEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;

@Controller
@RequestMapping(value = "/afastamento")
public class AfastamentoController {
	@Autowired
	private GenericDao<UserEntity> daoUser;
	@Autowired
	private List<UserEntity> listUser;
	@Autowired
	private List<AfastamentoEntity> list;
	@Autowired
	private UserEntity userEntity;

	@Autowired
	private GenericDao<AfastamentoEntity> daoAfastamento;
	@Autowired
	private AfastamentoEntity afastamentoEntity;
	@Autowired
	private List<AfastamentoEntity> listAfastamento;
	@Autowired
	private List<DocumentEntity> listDocument;
	@Autowired
	private GenericDao<DocumentEntity> daoDocument;

	@RequestMapping(value = { "/add" }, method = RequestMethod.GET)
	public ModelAndView requerer(ModelAndView model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		

		List<UserEntity> passa = new ArrayList<UserEntity>();
		List<UserEntity> listSubstitute = new ArrayList<UserEntity>();

		Map<String, Object> mapLocal = new HashMap<String, Object>();
		mapLocal.put("tipoCargo", "Local");
		mapLocal.put("status", "able");

		passa = daoUser.listarPropertyOrderBy(UserEntity.class, mapLocal, "and", "name");

		for (UserEntity u : passa) {
			if (u.getId() != userEntity.getId()) {
				listSubstitute.add(u);
			}
		}
		model.addObject("listSubstitute", listSubstitute);
		model.addObject("userEntity", userEntity);
		model.setViewName("afastamento/add");

		return model;

	}

	@RequestMapping(value = { "/pendentes" }, method = RequestMethod.GET)
	public ModelAndView pendentes(ModelAndView model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("requester.id", userEntity.getId());

		List<AfastamentoEntity> list = new ArrayList<AfastamentoEntity>();

		listAfastamento = daoAfastamento.listarProperty(AfastamentoEntity.class, map, "");
		for (AfastamentoEntity afasta : listAfastamento) {
			if ((afasta.getStatus().equals("Pendente Chefe Imediato"))
					|| (afasta.getStatus().equals("Pendente Administracao"))) {
				list.add(afasta);
			}
		}

		model.addObject("list", list);
		model.addObject("userEntity", userEntity);
		model.setViewName("afastamento/pendentes");

		return model;

	}

	@RequestMapping(value = { "/aprovados" }, method = RequestMethod.GET)
	public ModelAndView aprovados(ModelAndView model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("requester.id", userEntity.getId());

		List<AfastamentoEntity> list = new ArrayList<AfastamentoEntity>();

		Date hoje = new Date();
		hoje = Common.formata(hoje);
		
		listAfastamento = daoAfastamento.listDateAfter(AfastamentoEntity.class, hoje.toString(), "");
				
		for (AfastamentoEntity afasta : listAfastamento) {
			if (afasta.getStatus().equals("Aprovado")) {
				list.add(afasta);
			}
		}

		model.addObject("list", list);
		model.addObject("userEntity", userEntity);
		model.setViewName("afastamento/aprovados");

		return model;

	}
	@RequestMapping(value = { "/negados" }, method = RequestMethod.GET)
	public ModelAndView negados(ModelAndView model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("requester.id", userEntity.getId());
		map.put("status", "Negado");

		
		
		listAfastamento = daoAfastamento.listarProperty(AfastamentoEntity.class, map, "and");
				
		

		model.addObject("list", listAfastamento);
		model.addObject("userEntity", userEntity);
		model.setViewName("afastamento/negados");

		return model;

	}

	@RequestMapping(value = "/requerer", method = RequestMethod.POST)
	public ModelAndView requerer(AfastamentoEntity afastamento, MultipartFile file, HttpServletRequest request,
			ModelAndView model, String requesterID, String substituteID, String de, String ate) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		

		if (afastamento.getId() == null || afastamento.getId().equals(null)) {
			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				if (file.getSize() > 20971520) {
					System.out.println("Arquivo maior que 20 mb");
					model.addObject("erro", " Arquivo n�o pode ser maior que 20 MB");

					model.setViewName("afastamento/manual");
					return model;
				}

			}
			afastamento.setStatus("Pendente Chefe Imediato");
			model = salvarrequest(afastamento, model, de, ate, requesterID, substituteID, file);
			return model;

		} else {
			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				if (file.getSize() > 20971520) {
					System.out.println("Arquivo maior que 20 mb");
					model.addObject("erro", " Arquivo n�o pode ser maior que 20 MB");

					afastamentoEntity = daoAfastamento.buscaId(AfastamentoEntity.class, afastamento.getId());

					Map<String, Object> mapdoc = new HashMap<String, Object>();
					mapdoc.put("afastamentoEntity.id", afastamentoEntity.getId());

					listDocument = daoDocument.listarProperty(DocumentEntity.class, mapdoc, "and");

					model.addObject("listDocument", listDocument);

					model.addObject("afastamentoEntity", afastamentoEntity);

					model.setViewName("afastamento/editmanual");
					return model;
				}

			}
			// model = updatemanual(afastamento, model, de, ate, file);
			return model;

		}

	}

	public ModelAndView salvarrequest(AfastamentoEntity afastamento, ModelAndView model, String de, String ate,
			String requesterID, String substituteID, MultipartFile file) {

		UserEntity userRequester = new UserEntity();
		UserEntity userSubstituto = new UserEntity();

		userRequester = daoUser.buscaId(UserEntity.class, Integer.parseInt(requesterID));
		userSubstituto = daoUser.buscaId(UserEntity.class, Integer.parseInt(substituteID));

		Date dataDe = Common.passaDate(de);
		Date dataAte = Common.passaDate(ate);

		afastamento.setRequester(userRequester);
		afastamento.setSubstitute(userSubstituto);

		afastamento.setDateStart(dataDe);
		afastamento.setDateEnd(dataAte);

		Date hoje = Common.hoje();
		afastamento.setDateRequest(hoje);

		String uuid = Common.geraUUID();
		afastamento.setUuid(uuid);

		daoAfastamento.saveUpdate(afastamento);

		if (file.toString() != null && !file.getOriginalFilename().equals("")) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("uuid", uuid);
			afastamentoEntity = daoAfastamento.findByProperty(AfastamentoEntity.class, map, "and");

			Common.documentoAfastamento(userRequester, file, afastamentoEntity);

		}

		model.setViewName("redirect:/afastamento/pendentes");

		return model;
	}
}
