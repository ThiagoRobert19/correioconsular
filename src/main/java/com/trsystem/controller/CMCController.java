package com.trsystem.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AtosNotariaisEntity;
import com.trsystem.model.CarteiraEntity;
import com.trsystem.model.DocumentEntity;
import com.trsystem.model.DocumentStatusEntity;
import com.trsystem.model.ErrorEntity;
import com.trsystem.model.ObservationHistoryEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;

@Controller
@RequestMapping(value = "/cmc")
public class CMCController {

	@Autowired
	private CarteiraEntity carteiraEntity;
	@Autowired
	private List<CarteiraEntity> listCarteira;
	@Autowired
	private GenericDao<CarteiraEntity> daoCarteira;

	@Autowired
	private UserEntity userEntity;
	@Autowired
	private GenericDao<DocumentStatusEntity> daoDocumentStatus;

	@Autowired
	private DocumentStatusEntity documentStatusEntity;
	@Autowired
	private List<DocumentStatusEntity> listStatus;
	@Autowired
	private List<ObservationHistoryEntity> listObservation;
	@Autowired
	private GenericDao<ObservationHistoryEntity> daoObservation;
	@Autowired
	private ObservationHistoryEntity observationEntity;
	@Autowired
	private DocumentEntity documentEntity;
	@Autowired
	private List<DocumentEntity> listDocument;
	@Autowired
	private GenericDao<DocumentEntity> daoDocument;
	@Autowired
	private ErrorEntity errorEntity;
	@Autowired
	private GenericDao<ErrorEntity> daoError;

	@RequestMapping(value = { "/add" }, method = RequestMethod.GET)
	public ModelAndView add(HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

		model.addObject("listStatus", listStatus);

		model.addObject("userEntity", userEntity);

		model.setViewName("cmc/add");

		return model;

	}

	@RequestMapping(value = { "/data" }, method = RequestMethod.GET)
	public ModelAndView data(HttpServletRequest request, ModelAndView model) {
		listCarteira = daoCarteira.list(CarteiraEntity.class);
		PagedListHolder<CarteiraEntity> pagedListHolder = new PagedListHolder<CarteiraEntity>(listCarteira);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(25);

		model.addObject("pagedListHolder", pagedListHolder);

		model.setViewName("cmc/data");

		return model;

	}

	@RequestMapping(value = "/getbyprotocoldata", method = RequestMethod.POST)
	public ModelAndView buscaProtoboloData(String protocolo, ModelAndView model, HttpServletRequest request) {

		listCarteira = daoCarteira.listByPropertyLike(CarteiraEntity.class, "protocol", protocolo.trim().toUpperCase());
		PagedListHolder<CarteiraEntity> pagedListHolder = new PagedListHolder<CarteiraEntity>(listCarteira);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(25);

		model.addObject("pagedListHolder", pagedListHolder);

		model.setViewName("cmc/data");

		return model;
	}

	@RequestMapping(value = "/getbynamedata", method = RequestMethod.POST)
	public ModelAndView buscaNameData(String nomeuser, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("personName", nomeuser.trim().toUpperCase());
		listCarteira = daoCarteira.listByPropertyLike(CarteiraEntity.class, "personName",
				nomeuser.trim().toUpperCase());
		PagedListHolder<CarteiraEntity> pagedListHolder = new PagedListHolder<CarteiraEntity>(listCarteira);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(25);

		model.addObject("pagedListHolder", pagedListHolder);

		model.setViewName("cmc/data");

		return model;

	}

	@RequestMapping(value = { "/data/next/{next}" }, method = RequestMethod.GET)
	public ModelAndView datanext(@PathVariable(value = "next") String next, HttpServletRequest request,
			ModelAndView model) {
		listCarteira = daoCarteira.list(CarteiraEntity.class);
		PagedListHolder<CarteiraEntity> pagedListHolder = new PagedListHolder<CarteiraEntity>(listCarteira);

		pagedListHolder.setPage(Integer.parseInt(next));
		pagedListHolder.setPageSize(25);

		model.addObject("pagedListHolder", pagedListHolder);
		model.setViewName("cmc/data");
		return model;

	}

	@RequestMapping(value = "/getbyprotocol", method = RequestMethod.POST)
	public ModelAndView buscaProtobolo(String protocolo, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("protocol", protocolo.trim().toUpperCase());

		if (daoCarteira.exist(CarteiraEntity.class, map, "and")) {

			carteiraEntity = daoCarteira.findByProperty(CarteiraEntity.class, map, "and");

			Map<String, Object> obs = new HashMap<String, Object>();
			obs.put("carteiraEntity.id", carteiraEntity.getId());
			listObservation = daoObservation.listarProperty(ObservationHistoryEntity.class, obs, "and");
			String obser = "";
			for (ObservationHistoryEntity hist : listObservation) {
				obser = obser + "\n" + hist.getDetail();
			}
			Map<String, Object> mapdoc = new HashMap<String, Object>();
			mapdoc.put("carteiraEntity.id", carteiraEntity.getId());
			listDocument = daoDocument.listarProperty(DocumentEntity.class, mapdoc, "and");

			listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

			model.addObject("listStatus", listStatus);
			model.addObject("listDocument", listDocument);
			model.addObject("listObservation", listObservation);
			model.addObject("carteiraEntity", carteiraEntity);
			model.addObject("obser", obser);

			model.setViewName("cmc/add");
			return model;

		} else {
			userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

			listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

			model.addObject("listStatus", listStatus);

			model.addObject("userEntity", userEntity);

			model.addObject("erro", "Protocolo n�o encontrado!");

			model.setViewName("cmc/add");
			return model;
		}

	}
	@RequestMapping(value = { "/quando/{texto}" }, method = RequestMethod.GET)
	public ModelAndView inseridosdata(@PathVariable(value = "texto") String texto, HttpServletRequest request,
			ModelAndView model) {
		Calendar cal1 = Calendar.getInstance();
		int year = cal1.get(Calendar.YEAR);
		Date hoje = new Date();
		if (texto.equals("hoje")) {
			hoje = Common.formata(hoje);
			listCarteira = daoCarteira.listDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry", hoje.toString(),
					hoje.toString());
		}
		if (texto.equals("semana")) {
			Calendar dateweek = Calendar.getInstance();

			dateweek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

			Date semanaInicio = Common.formata(dateweek.getTime());
			for (int i = 0; i < 6; i++) {
				dateweek.add(Calendar.DATE, 1);
			}

			Date semanaFim = Common.formata(dateweek.getTime());

			listCarteira = daoCarteira.listDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry",
					semanaInicio.toString(), semanaFim.toString());

		}
		if (texto.equals("mes")) {

			int firstDate = cal1.getActualMinimum(Calendar.DATE);
			int month = cal1.get(Calendar.MONTH);

			int lastDate = cal1.getActualMaximum(Calendar.DATE);

			cal1.set(year, month, firstDate);

			Date mesInicio = Common.formata(cal1.getTime());

			cal1.set(year, month, lastDate);

			Date mesFim = Common.formata(cal1.getTime());

			listCarteira = daoCarteira.listDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry",
					mesInicio.toString(), mesFim.toString());

		}
		if (texto.equals("ano")) {

			int diaum = 1;
			int monthJaneiro = 0;
			int monthdezembro = 11;
			int diaultimo = 31;

			cal1.set(year, monthJaneiro, diaum);

			Date primeiradata = Common.formata(cal1.getTime());

			cal1.set(year, monthdezembro, diaultimo);

			Date ultimadata = Common.formata(cal1.getTime());

			listCarteira = daoCarteira.listDateMaiorMenor(CarteiraEntity.class, "dateEntry", "dateEntry",
					primeiradata.toString(), ultimadata.toString());

		}
		if (!texto.equals("hoje") && texto.equals("semana") && texto.equals("mes") && texto.equals("ano")) {
			model.setViewName("redirect:/");

			return model;

		}

		model.addObject("textodata", texto);
		model.addObject("list", listCarteira);
		model.setViewName("cmc/inseridosdata");

		return model;

	}
	@RequestMapping(value = "/ver/{id}", method = RequestMethod.GET)
	public ModelAndView ver(@PathVariable(value = "id") String id, ModelAndView model) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoCarteira.exist(CarteiraEntity.class, map, "and")) {

			carteiraEntity = daoCarteira.findByProperty(CarteiraEntity.class, map, "and");

			Map<String, Object> obs = new HashMap<String, Object>();
			obs.put("carteiraEntity.id", carteiraEntity.getId());
			listObservation = daoObservation.listarProperty(ObservationHistoryEntity.class, obs, "and");
			String obser = "";
			for (ObservationHistoryEntity hist : listObservation) {
				obser = obser + "\n" + hist.getDetail();
			}
			Map<String, Object> mapdoc = new HashMap<String, Object>();
			mapdoc.put("carteiraEntity.id", carteiraEntity.getId());
			listDocument = daoDocument.listarProperty(DocumentEntity.class, mapdoc, "and");

			listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

			model.addObject("listStatus", listStatus);
			model.addObject("listDocument", listDocument);
			model.addObject("listObservation", listObservation);
			model.addObject("carteiraEntity", carteiraEntity);
			model.addObject("obser", obser);

			model.setViewName("cmc/add");
			return model;

		} else {

			model.setViewName("redirect:/cmc/data");
			return model;
		}

	}

	@RequestMapping(value = "/saveupdate", method = RequestMethod.POST)
	public ModelAndView saveupdate(CarteiraEntity carteira, String dataentrada, String datasaida, String statusID,
			String userID, String observa, MultipartFile file, HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		System.out.println("ENTROU SAVEUPDATE");
		if (carteira.getId() == null || carteira.getId().equals(null)) {
			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				if (file.getSize() > 20971520) {
					System.out.println("Arquivo maior que 20 mb");
					model.addObject("erro", " Arquivo n�o pode ser maior que 20 MB");

					model.setViewName("cmc/add");
					return model;
				}

			}
			try {
				if (statusID == null || statusID.equals("")) {

					model = salvar(carteira, model, dataentrada, datasaida, statusID, file, observa, userEntity);
					return model;

				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("id", Integer.parseInt(statusID));
					if (daoDocumentStatus.exist(DocumentStatusEntity.class, map, "and")) {
						model = salvar(carteira, model, dataentrada, datasaida, statusID, file, observa, userEntity);
						return model;
					} else {
						model.setViewName("redirect:/");
						return model;
					}
				}

			} catch (Exception e) {

				String uuid = Common.geraUUID();
				errorEntity.setUuid(uuid);
				errorEntity.setDetail(e.getMessage().toString());
				errorEntity.setDateEntry(Common.hoje());
				errorEntity.setHourEntry(Common.hora());
				errorEntity.setUserEntity(userEntity);
				daoError.saveUpdate(errorEntity);

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("uuid", uuid);
				errorEntity = daoError.findByProperty(ErrorEntity.class, map, "");
				model.addObject("errorEntity", errorEntity);
				model.setViewName("erro/show");

				return model;

			}

		} else {

			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				if (file.getSize() > 20971520) {
					System.out.println("Arquivo maior que 20 mb");
					model.addObject("erro", " Arquivo n�o pode ser maior que 20 MB");

					carteiraEntity = daoCarteira.buscaId(CarteiraEntity.class, carteira.getId());

					Map<String, Object> obs = new HashMap<String, Object>();
					obs.put("carteiraEntity.id", carteiraEntity.getId());
					listObservation = daoObservation.listarProperty(ObservationHistoryEntity.class, obs, "and");

					Map<String, Object> mapdoc = new HashMap<String, Object>();
					mapdoc.put("carteiraEntity.id", carteiraEntity.getId());
					listDocument = daoDocument.listarProperty(DocumentEntity.class, mapdoc, "and");

					listStatus = daoDocumentStatus.list(DocumentStatusEntity.class);

					model.addObject("listStatus", listStatus);
					model.addObject("listDocument", listDocument);

					model.addObject("carteiraEntity", carteiraEntity);
					model.addObject("listObservation", listObservation);

					model.setViewName("cmc/add");
					return model;
				}

			}

			try {
				if (statusID == null || statusID.equals("")) {

					model = update(carteira, model, dataentrada, datasaida, statusID, file, observa, userEntity);
					return model;
				} else {

					Map<String, Object> map = new HashMap<String, Object>();
					map.put("id", Integer.parseInt(statusID));
					if (daoDocumentStatus.exist(DocumentStatusEntity.class, map, "and")) {
						model = update(carteira, model, dataentrada, datasaida, statusID, file, observa, userEntity);
						return model;
					} else {
						model.setViewName("redirect:/");
						return model;
					}
				}

			} catch (Error e) {

				String uuid = Common.geraUUID();
				errorEntity.setUuid(uuid);
				errorEntity.setDetail(e.getMessage().toString());
				errorEntity.setDateEntry(Common.hoje());
				errorEntity.setHourEntry(Common.hora());
				errorEntity.setUserEntity(userEntity);
				daoError.saveUpdate(errorEntity);

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("uuid", uuid);
				errorEntity = daoError.findByProperty(ErrorEntity.class, map, "");
				model.addObject("errorEntity", errorEntity);
				model.setViewName("erro/show");

				return model;

			}

		}
	}

	@RequestMapping(value = { "/anexos/{id}" }, method = RequestMethod.GET)
	public void anexo(@PathVariable(value = "id") String id, HttpServletRequest request, HttpServletResponse response,
			ModelAndView model) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoDocument.exist(DocumentEntity.class, map, "and")) {
			documentEntity = daoDocument.buscaId(DocumentEntity.class, Integer.parseInt(id));

			try {

				File arquivo = new File(documentEntity.getLocalDocumento());
				String nom = arquivo.getName();

				java.nio.file.Path path = arquivo.toPath();

				response.setHeader("Content-Disposition", "attachment; filename=\"" + nom + "\"");

				OutputStream output = null;
				output = response.getOutputStream();
				Files.copy(path, output);

			} catch (IOException e) {

				String uuid = Common.geraUUID();
				errorEntity.setUuid(uuid);
				errorEntity.setDetail(e.getMessage().toString());
				errorEntity.setDateEntry(Common.hoje());
				errorEntity.setHourEntry(Common.hora());
				errorEntity.setUserEntity(userEntity);
				daoError.saveUpdate(errorEntity);


			}

		}

	}

	@RequestMapping(value = { "/anexosdelete/{id}" }, method = RequestMethod.GET)
	public ModelAndView anexodelete(@PathVariable(value = "id") String id, HttpServletRequest request,
			HttpServletResponse response, ModelAndView model) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));
		Integer ida = 0;
		if (daoDocument.exist(DocumentEntity.class, map, "and")) {
			documentEntity = daoDocument.findByProperty(DocumentEntity.class, map, "and");
			ida = documentEntity.getPassaporteEntity().getId();
			Common.documentoDelete(documentEntity);
			daoDocument.delete(DocumentEntity.class, map, "and");

		}
		model.setViewName("redirect:/cmc/ver/" + ida);
		return model;

	}

	@RequestMapping(value = { "/obsdelete/{id}" }, method = RequestMethod.GET)
	public ModelAndView obsdelete(@PathVariable(value = "id") String id, HttpServletRequest request,
			HttpServletResponse response, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));
		Integer ida = 0;
		if (daoObservation.exist(ObservationHistoryEntity.class, map, "and")) {
			observationEntity = daoObservation.findByProperty(ObservationHistoryEntity.class, map, "and");
			ida = observationEntity.getCarteiraEntity().getId();
			daoObservation.delete(ObservationHistoryEntity.class, map, "and");

		}
		model.setViewName("redirect:/cmc/ver/" + ida);
		return model;

	}

	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "id") String id, HttpServletRequest request,
			HttpServletResponse response, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoCarteira.exist(CarteiraEntity.class, map, "and")) {

			Map<String, Object> mapPass = new HashMap<String, Object>();
			mapPass.put("carteiraEntity.id", Integer.parseInt(id));

			daoObservation.delete(ObservationHistoryEntity.class, mapPass, "and");
			daoDocument.delete(DocumentEntity.class, mapPass, "and");

			daoCarteira.delete(CarteiraEntity.class, map, "and");
		}
		model.setViewName("redirect:/cmc/data");
		return model;

	}

	public ModelAndView salvar(CarteiraEntity carteira, ModelAndView model, String dataentrada, String datasaida,
			String statusID, MultipartFile file, String observa, UserEntity userEntity) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("protocol", carteira.getProtocol());
		if (daoCarteira.exist(CarteiraEntity.class, map, "and")) {

			model.addObject("erro", " N�o � poss�vel adicionar a Carteira, ja Existe CMC com o mesmo protocolo!");

			model.setViewName("cmc/add");
			return model;
		} else {
			if (statusID == null || statusID.equals("")) {
				System.out.println("N�o faz nada");

			} else {
				documentStatusEntity = daoDocumentStatus.buscaId(DocumentStatusEntity.class,
						Integer.parseInt(statusID));
				carteira.setDocumentStatus(documentStatusEntity);
			}

			carteira.setAtendente(userEntity);
			String protocolo = carteira.getProtocol();

			if (datasaida != null && !datasaida.equals("")) {

				carteira.setDateDeparture(Common.passaDate(datasaida));
			}
			carteira.setDateEntry(Common.passaDate(dataentrada));
			daoCarteira.saveUpdate(carteira);

			Map<String, Object> pegapass = new HashMap<String, Object>();
			pegapass.put("carteiraEntity.id", carteiraEntity.getId());

			Map<String, Object> mapprotocolo = new HashMap<String, Object>();
			mapprotocolo.put("protocol", protocolo);

			carteiraEntity = daoCarteira.findByProperty(CarteiraEntity.class, mapprotocolo, "and");

			if (file.toString() != null && !file.getOriginalFilename().equals("")) {

				Common.documentoCarteira(carteiraEntity, file, documentEntity, daoDocument);

			}
			if (observa.trim() != null && !observa.trim().equals("")) {
				observationEntity.setDateObervation(Common.hojeHora());
				observationEntity.setObservation(observa.trim());
				observationEntity.setCarteiraEntity(carteiraEntity);

				observationEntity.setTipoDocumento("carteira");
				observationEntity.setUserEntity(userEntity);

				SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String stringDate = DateFor.format(observationEntity.getDateObervation());

				observationEntity.setDetail("Data e Hora: " + stringDate + "| Atendente: " + userEntity.getName()
						+ "| Obs: " + observa.trim());

				daoObservation.saveUpdate(observationEntity);

			}
			model.setViewName("redirect:/cmc/add");

			return model;
		}
	}

	public ModelAndView update(CarteiraEntity carteira, ModelAndView model, String dataentrada, String datasaida,
			String statusID, MultipartFile file, String observa, UserEntity userEntity) {
		
		carteiraEntity = daoCarteira.buscaId(CarteiraEntity.class, carteira.getId());
		
		if (file.toString() != null && !file.getOriginalFilename().equals("")) {
			DocumentEntity doc = new DocumentEntity();
			Common.documentoCarteira(carteiraEntity, file, doc, daoDocument);

		}
		
		if (observa.trim() != null && !observa.trim().equals("")) {
		
			ObservationHistoryEntity observacao = new ObservationHistoryEntity();

			observacao.setDateObervation(Common.hojeHora());
		
			observacao.setObservation(observa.trim());
		
			observacao.setCarteiraEntity(carteiraEntity);
			
			observacao.setTipoDocumento("carteira");
			observacao.setUserEntity(userEntity);
		
			SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			String stringDate = DateFor.format(observacao.getDateObervation());
			
			observacao.setDetail("Data e Hora: " + stringDate + " | Atendente: " + userEntity.getName() + " | Obs: "
					+ observa.trim());

			daoObservation.saveUpdate(observacao);
			
		}

		if (statusID == null || statusID.equals("")) {
			System.out.println("N�o faz nada");

		} else {
		
			documentStatusEntity = daoDocumentStatus.buscaId(DocumentStatusEntity.class, Integer.parseInt(statusID));

			if (carteiraEntity.getDocumentStatus() != null) {

				if (carteiraEntity.getDocumentStatus().getId() != Integer.parseInt(statusID)) {

					ObservationHistoryEntity observacao = new ObservationHistoryEntity();

					observacao.setDateObervation(Common.hojeHora());

					observacao.setObservation("Status alterado de: " + carteiraEntity.getDocumentStatus().getName()
							+ " para: " + documentStatusEntity.getName());

					observacao.setCarteiraEntity(carteiraEntity);

					observacao.setTipoDocumento("carteira");
					observacao.setUserEntity(userEntity);

					SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					String stringDate = DateFor.format(observacao.getDateObervation());

					observacao.setDetail("Data e Hora: " + stringDate + " | Atendente: " + userEntity.getName()
							+ " | Obs: Status alterado de: " + carteiraEntity.getDocumentStatus().getName() + " para: "
							+ documentStatusEntity.getName());

					daoObservation.saveUpdate(observacao);

				}

			}
			carteiraEntity.setDocumentStatus(documentStatusEntity);
		}

		if (dataentrada != null && !dataentrada.equals("")) {
			Date passa2 = Common.passaDate(dataentrada);
			carteiraEntity.setDateEntry(passa2);
		}
		if (datasaida != null && !datasaida.equals("")) {
			Date passa2 = Common.passaDate(datasaida);
			carteiraEntity.setDateDeparture(passa2);
		}

		carteiraEntity.setPersonAddress(carteira.getPersonAddress());
		carteiraEntity.setPersonEmail(carteira.getPersonEmail());
		carteiraEntity.setPersonName(carteira.getPersonName());
		carteiraEntity.setPersonPhone(carteira.getPersonPhone());
		carteiraEntity.setTrackingNumberEnvio(carteira.getTrackingNumberEnvio());
		carteiraEntity.setTrackingNumberRetorno(carteira.getTrackingNumberRetorno());

		daoCarteira.saveUpdate(carteiraEntity);

		model.setViewName("redirect:/cmc/ver/" + carteira.getId());

		return model;
	}
}
