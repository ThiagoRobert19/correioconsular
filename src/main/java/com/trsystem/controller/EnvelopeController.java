package com.trsystem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.EnvelopeEntity;

import com.trsystem.model.UserEntity;


@Controller
@RequestMapping(value = "/envelope")
public class EnvelopeController {
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private GenericDao<EnvelopeEntity> daoEnvelope;
	@Autowired
	private List<EnvelopeEntity> list;
	@Autowired
	private EnvelopeEntity envelopeEntity;



	@RequestMapping(value = { "/data" }, method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");
			return model;

		}
		list=daoEnvelope.list(EnvelopeEntity.class);

		model.addObject("list", list);
		model.setViewName("envelope/data");
		return model;

	}

	@RequestMapping(value = { "/add" }, method = RequestMethod.GET)
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		model.setViewName("envelope/add");

		return model;

	}

	@RequestMapping(value = "/saveupdate", method = RequestMethod.POST)
	public ModelAndView saveupdate(EnvelopeEntity envelope, HttpServletRequest request,
			ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", envelope.getName());
		if (envelope.getId() == null || envelope.getId().equals(null)) {

			if (daoEnvelope.exist(EnvelopeEntity.class, map, "and")) {

				model.addObject("erro", " N�o � poss�vel adicionar o Envelope, ja Existe Envelope com o mesmo nome!");

				model.setViewName("envelope/add");
				return model;
			}

		}

		daoEnvelope.saveUpdate(envelope);

		model.setViewName("redirect:/envelope/data");

		return model;

	}

	@RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoEnvelope.exist(EnvelopeEntity.class, map, "and")) {
			envelopeEntity = daoEnvelope.findByProperty(EnvelopeEntity.class, map, "and");
			model.addObject("envelopeEntity", envelopeEntity);
			model.setViewName("/envelope/edit");
			return model;
		} else {
			model.setViewName("redirect:/envelope/data");
			return model;
		}

	}

}
