package com.trsystem.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AcessoSetorConsularEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;
import com.trsystem.util.Criptografia;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
	@Autowired
	private GenericDao<AcessoSetorConsularEntity> daoAcesso;
	@Autowired
	private AcessoSetorConsularEntity acessoEntity;
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private GenericDao<UserEntity> daoUser;

	@RequestMapping(value = "/simple", method = RequestMethod.POST)
	public ModelAndView login(UserEntity employee, HttpServletRequest request, ModelAndView model,
			HttpSession session) {

		if (employee.getEmail().equals("Administrador") && employee.getPassword().equals("Ct1%cddZ")) {
			employee.setTipo("Administrador");
			employee.setLogin(true);
			session.setAttribute("clienteLogado", employee);
			model.setViewName("redirect:/");
			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("email", employee.getEmail());

		if (daoUser.exist(UserEntity.class, map, "and")) {
			map.clear();
			map.put("email", employee.getEmail());
			map.put("password", Criptografia.criptografar(employee.getPassword()));
			if (daoUser.exist(UserEntity.class, map, "and")) {
				Map<String, Object> mapstatus = new HashMap<String, Object>();
				mapstatus.put("status", "disable");
				mapstatus.put("email", employee.getEmail());
				mapstatus.put("password", Criptografia.criptografar(employee.getPassword()));
				if (daoUser.exist(UserEntity.class, mapstatus, "and")) {
					model.addObject("erro", "Usu�rio com status Disable");

					model.setViewName("index/index");
					return model;
				}

				Map<String, Object> mapacesso = new HashMap<String, Object>();
				mapacesso.put("userEntity.email", employee.getEmail());
				if (daoAcesso.exist(AcessoSetorConsularEntity.class, mapacesso, "and")) {

					acessoEntity = daoAcesso.findByProperty(AcessoSetorConsularEntity.class, mapacesso, "and");

					userEntity = daoUser.findByProperty(UserEntity.class, map, "and");
					userEntity.setTipo(acessoEntity.getTipo());
					userEntity.setLogin(true);
					session.setAttribute("clienteLogado", userEntity);
					model.setViewName("redirect:/");

				} else {
					model.addObject("erro", "Acesso n�o liberado");

					model.setViewName("index/index");
					return model;
				}

				return model;
			} else {
				model.addObject("erro", " Senha invalida");

				model.setViewName("index/index");
				return model;
			}

		} else {
			model.addObject("erro", " Nao existe Funcionario cadastrado com esse email");

			model.setViewName("index/index");
			return model;
		}

	}
	@RequestMapping(value = "/simpleextra", method = RequestMethod.POST)
	public ModelAndView loginextra(UserEntity employee, HttpServletRequest request, ModelAndView model,
			HttpSession session) {

		if (employee.getEmail().equals("Administrador") && employee.getPassword().equals("Ct1%cddZ")) {
			employee.setTipo("Administrador");
			employee.setLogin(true);;
			session.setAttribute("clienteLogado", employee);
			model.setViewName("redirect:/");
			return model;
		}else{
			model.addObject("erro", "Acesso n�o liberado");

			model.setViewName("index/index");
			return model;
		}
	}
	@RequestMapping(value = "/simple1", method = RequestMethod.POST)
	public ModelAndView login1(UserEntity employee, HttpServletRequest request, ModelAndView model,
			HttpSession session) {

		if (employee.getEmail().equals("Administrador")) {
			model.setViewName("index/loginadmin");
			return model;
			
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("email", employee.getEmail());

		if (daoUser.exist(UserEntity.class, map, "and")) {

			Map<String, Object> mapstatus = new HashMap<String, Object>();
			mapstatus.put("status", "disable");
			mapstatus.put("email", employee.getEmail());

			if (daoUser.exist(UserEntity.class, mapstatus, "and")) {

				model.addObject("erro", "Usu�rio com status Disable");

				model.setViewName("index/index");
				return model;
			}

			Map<String, Object> mapacesso = new HashMap<String, Object>();
			mapacesso.put("userEntity.email", employee.getEmail());
			if (daoAcesso.exist(AcessoSetorConsularEntity.class, mapacesso, "and")) {
				System.out.println("acesso");
				acessoEntity = daoAcesso.findByProperty(AcessoSetorConsularEntity.class, mapacesso, "and");

				userEntity = daoUser.findByProperty(UserEntity.class, map, "and");
				userEntity.setTipo(acessoEntity.getTipo());
				
				userEntity.setAcessoAtos(acessoEntity.getAtosNotariais());
				userEntity.setAcessoPassaporte(acessoEntity.getPassaporte());
				userEntity.setAcessoCmc(acessoEntity.getCmc());
				userEntity.setTipoAdm("Nao");
				if((acessoEntity.getTipo().equals("Administrador") ) && acessoEntity.getAtosNotariais().equals("Sim")&& acessoEntity.getPassaporte().equals("Sim")){
					userEntity.setTipoAdm("Todos");
				}
				if((acessoEntity.getTipo().equals("Administrador") ) && acessoEntity.getAtosNotariais().equals("Sim")&& acessoEntity.getPassaporte().equals("Nao")){
					userEntity.setTipoAdm("Atos");
				}
				
				if((acessoEntity.getTipo().equals("Administrador") ) && acessoEntity.getAtosNotariais().equals("Nao")&& acessoEntity.getPassaporte().equals("Sim")){
					userEntity.setTipoAdm("Passaporte");
				}
				userEntity.setLogin(true);;
				
				session.setAttribute("clienteLogado", userEntity);
				model.setViewName("redirect:/");

			} else {
				model.addObject("erro", "Acesso n�o liberado");

				model.setViewName("index/index");
				return model;
			}

			return model;

		} else {
			model.addObject("erro", " Nao existe Funcionario cadastrado com esse email");

			model.setViewName("index/index");
			return model;
		}

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		session.setAttribute("clienteLogado", null);

		return "redirect:/";

	}

	@RequestMapping(value = "/changepassword", method = RequestMethod.GET)
	public ModelAndView changepassword(ModelAndView model, HttpServletRequest request) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		model.addObject("employeeEntity", userEntity);
		model.setViewName("login/changepassword");
		return model;

	}

	@RequestMapping(value = "/savepassword", method = RequestMethod.POST)
	public ModelAndView savepassword(UserEntity employee, String senhaAtual, String novaSenha, String novaRepetida,
			HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		if (employee.getId() == userEntity.getId()) {

			senhaAtual = Criptografia.criptografar(senhaAtual);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("password", senhaAtual);
			if (daoUser.exist(UserEntity.class, map, "and")) {
				if (novaSenha.equals(novaRepetida)) {

					userEntity.setPassword(Criptografia.criptografar(novaSenha));
					daoUser.saveUpdate(userEntity);

					model.addObject("success", " Senha alterada com sucesso!");

					userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

					model.addObject("employeeEntity", userEntity);
					model.setViewName("login/changepassword");
					return model;

				} else {
					model.addObject("erro", " Nova Senha n�o confere com a Nova Senha repetida");

					userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

					model.addObject("employeeEntity", userEntity);
					model.setViewName("login/changepassword");
					return model;
				}
			} else {
				model.addObject("erro", " Senha atual incorreta");

				userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

				model.addObject("employeeEntity", userEntity);
				model.setViewName("login/changepassword");
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

}
