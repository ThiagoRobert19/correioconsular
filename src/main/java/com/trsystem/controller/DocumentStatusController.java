package com.trsystem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.DocumentStatusEntity;
import com.trsystem.model.ErrorEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;
import com.trsystem.util.Criptografia;

@Controller
@RequestMapping(value = "/documentstatus")
public class DocumentStatusController {
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private GenericDao<DocumentStatusEntity> daoDocumentStatus;
	@Autowired
	private List<DocumentStatusEntity> list;
	@Autowired
	private DocumentStatusEntity documentStatusEntity;

	@Autowired
	private ErrorEntity errorEntity;
	@Autowired
	private GenericDao<ErrorEntity> daoError;

	@RequestMapping(value = { "/data" }, method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");
			return model;

		}
		list = daoDocumentStatus.list(DocumentStatusEntity.class);

		model.addObject("list", list);
		model.setViewName("documentstatus/data");
		return model;

	}

	@RequestMapping(value = { "/add" }, method = RequestMethod.GET)
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		model.setViewName("documentstatus/add");

		return model;

	}

	@RequestMapping(value = "/saveupdate", method = RequestMethod.POST)
	public ModelAndView saveupdate(DocumentStatusEntity documentstatus, HttpServletRequest request,
			ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", documentstatus.getName());
		if (documentstatus.getId() == null || documentstatus.getId().equals(null)) {

			if (daoDocumentStatus.exist(DocumentStatusEntity.class, map, "and")) {

				model.addObject("erro", " N�o � poss�vel adicionar o Status, ja Existe Status com o mesmo nome!");

				model.setViewName("documentstatus/add");
				return model;
			}

		}

		daoDocumentStatus.saveUpdate(documentstatus);

		model.setViewName("redirect:/documentstatus/data");

		return model;

	}

	@RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoDocumentStatus.exist(DocumentStatusEntity.class, map, "and")) {
			documentStatusEntity = daoDocumentStatus.findByProperty(DocumentStatusEntity.class, map, "and");
			model.addObject("documentStatusEntity", documentStatusEntity);
			model.setViewName("/documentstatus/edit");
			return model;
		} else {
			model.setViewName("redirect:/documentstatus/data");
			return model;
		}

	}

}
