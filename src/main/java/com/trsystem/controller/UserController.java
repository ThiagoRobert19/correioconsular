package com.trsystem.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AcessoSetorConsularEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;
import com.trsystem.util.Criptografia;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private GenericDao<UserEntity> daoUser;
	@Autowired
	private List<UserEntity> list;
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private List<AcessoSetorConsularEntity> listAcesso;
	@Autowired
	private GenericDao<AcessoSetorConsularEntity> daoAcesso;
	@Autowired
	private AcessoSetorConsularEntity acessoEntity;
	private Common common;

	@RequestMapping(value = { "/acesso" }, method = RequestMethod.GET)
	public ModelAndView acesso(ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.status", "able");

		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map, "and");

		model.addObject("listAcesso", listAcesso);

		model.setViewName("user/acesso");

		return model;

	}

	@RequestMapping(value = { "/permit" }, method = RequestMethod.GET)
	public ModelAndView permit(ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "able");
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("userEntity.status", "able");
		list = daoUser.listarProperty(UserEntity.class, map, "and");
		listAcesso = daoAcesso.listarProperty(AcessoSetorConsularEntity.class, map1, "and");
		
		List<UserEntity> listu = new ArrayList<UserEntity>();
		
		for (UserEntity user : list) {
		 boolean resp=false;
			for (AcessoSetorConsularEntity ac : listAcesso) {
				if (user.getId() == ac.getUserEntity().getId()) {
					resp=true;
					
				}
			}
			if(!resp){
				listu.add(user);
			}
		}

		model.addObject("listExtra", listu);
		model.setViewName("user/permit");

		return model;

	}

	@RequestMapping(value = "/givepermit", method = RequestMethod.POST)
	public ModelAndView givepermit(String userID,String acessoPassaporte,String acessoAtos,String acessoCMC, String tipo, HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userEntity.id", Integer.parseInt(userID));
		if (!daoAcesso.exist(AcessoSetorConsularEntity.class, map, "and")) {
			UserEntity user = new UserEntity();
			user = daoUser.buscaId(UserEntity.class, Integer.parseInt(userID));
			AcessoSetorConsularEntity acesso = new AcessoSetorConsularEntity();
			user.setAcessoAtos(acessoAtos);
			user.setAcessoPassaporte(acessoPassaporte);
			user.setAcessoCmc(acessoCMC);
			
			
			acesso.setTipo(tipo);
			acesso.setUserEntity(user);
			acesso.setAtosNotariais(acessoAtos);
			acesso.setPassaporte(acessoPassaporte);
			acesso.setCmc(acessoCMC);
			daoAcesso.saveUpdate(acesso);
			
			daoUser.saveUpdate(user);
		}

		model.setViewName("redirect:/user/acesso");

		return model;

	}

	@RequestMapping(value = { "/revoke/{id}" }, method = RequestMethod.GET)
	public ModelAndView revoke(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoAcesso.exist(AcessoSetorConsularEntity.class, map, "and")) {
			acessoEntity=daoAcesso.findByProperty(AcessoSetorConsularEntity.class, map, "and");
			UserEntity user = new UserEntity();
			user=acessoEntity.getUserEntity();
			
			user.setAcessoAtos("N�o");
			user.setAcessoPassaporte("N�o");
			user.setAcessoCmc("N�o");
			daoUser.saveUpdate(user);
			daoAcesso.delete(AcessoSetorConsularEntity.class, map, "and");

			model.setViewName("redirect:/user/acesso");
			return model;
		} else {
			model.setViewName("redirect:/user/acesso");
			return model;
		}

	}

	@RequestMapping(value = { "/changeaccess/{id}" }, method = RequestMethod.GET)
	public ModelAndView changeaccess(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoAcesso.exist(AcessoSetorConsularEntity.class, map, "and")) {
			acessoEntity = daoAcesso.findByProperty(AcessoSetorConsularEntity.class, map, "and");
			model.addObject("acessoEntity", acessoEntity);
			model.setViewName("user/changeaccess");
			return model;
		} else {
			model.setViewName("redirect:/user/acesso");
			return model;
		}

	}

	@RequestMapping(value = "/changepermit", method = RequestMethod.POST)
	public ModelAndView changepermit(String acessoID, String alteratipo, String tipoNovo, String alterapassaporte,
			String tipoPassaporte, String alteraatos, String tipoAtos,String alteracmc, String tipoCMC, HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(acessoID));
		if (daoAcesso.exist(AcessoSetorConsularEntity.class, map, "and")) {
			acessoEntity = daoAcesso.findByProperty(AcessoSetorConsularEntity.class, map, "and");

			UserEntity user = new UserEntity();
			user=acessoEntity.getUserEntity();
			
			if (alteratipo.equals("Sim")) {
				acessoEntity.setTipo(tipoNovo);
			}
			if (alterapassaporte.equals("Sim")) {
				acessoEntity.setPassaporte(tipoPassaporte);

			}
			if (alteraatos.equals("Sim")) {
				acessoEntity.setAtosNotariais(tipoAtos);

			}
			if (alteracmc.equals("Sim")) {
				acessoEntity.setCmc(tipoCMC);

			}
			user.setAcessoAtos(acessoEntity.getAtosNotariais());
			user.setAcessoCmc(acessoEntity.getCmc());
			user.setAcessoPassaporte(acessoEntity.getPassaporte());
			
			daoUser.saveUpdate(user);
			
			daoAcesso.saveUpdate(acessoEntity);

		}

		model.setViewName("redirect:/user/acesso");

		return model;

	}

	@RequestMapping(value = { "/repassword/{id}" }, method = RequestMethod.GET)
	public ModelAndView repassword(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoUser.exist(UserEntity.class, map, "and")) {
			String senhaGerada = common.geraSenha();

			UserEntity user = new UserEntity();
			user = daoUser.findByProperty(UserEntity.class, map, "and");

			user.setPassword(Criptografia.criptografar(senhaGerada));

			daoUser.saveUpdate(user);

			user = daoUser.findByProperty(UserEntity.class, map, "and");
			model.addObject("senhaGerada", senhaGerada);
			model.addObject("userEntity", user);

			model.setViewName("user/senhaGerada");
			return model;
		} else {
			model.setViewName("redirect:/user/acesso");
			return model;
		}

	}

	@RequestMapping(value = { "/atualizaacessos" }, method = RequestMethod.GET)
	public ModelAndView atualizaacessos(ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		daoAcesso.updatetabela(AcessoSetorConsularEntity.class, "passaporte", "Sim", "tipo", "Atendente");
		daoAcesso.updatetabela(AcessoSetorConsularEntity.class, "atosNotariais", "Nao", "tipo", "Atendente");
		daoAcesso.updatetabela(AcessoSetorConsularEntity.class, "passaporte", "Sim", "tipo", "Administrador");
		daoAcesso.updatetabela(AcessoSetorConsularEntity.class, "atosNotariais", "Nao", "tipo", "Administrador");

		daoUser.updatetabela(UserEntity.class, "acessoPassaporte", "Sim", "", "");
		daoUser.updatetabela(UserEntity.class, "acessoAtos", "Sim", "", "");

		model.setViewName("redirect:/");

		return model;

	}
}
