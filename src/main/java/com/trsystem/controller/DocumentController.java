package com.trsystem.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.DocumentEntity;
import com.trsystem.model.ErrorEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;

@Controller
@RequestMapping(value = "/document")
public class DocumentController {

	@Autowired
	private DocumentEntity documentEntity;
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private List<DocumentEntity> listDocument;
	@Autowired
	private GenericDao<DocumentEntity> daoDocument;

	@Autowired
	private ErrorEntity errorEntity;
	@Autowired
	private GenericDao<ErrorEntity> daoError;

	@RequestMapping(value = { "/anexos/{id}" }, method = RequestMethod.GET)
	public void anexo(@PathVariable(value = "id") String id, HttpServletRequest request, HttpServletResponse response,
			ModelAndView model) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoDocument.exist(DocumentEntity.class, map, "and")) {
			documentEntity = daoDocument.buscaId(DocumentEntity.class, Integer.parseInt(id));

			try {

				File arquivo = new File(documentEntity.getLocalDocumento());
				String nom = arquivo.getName();

				java.nio.file.Path path = arquivo.toPath();

				response.setHeader("Content-Disposition", "attachment; filename=\"" + nom + "\"");

				OutputStream output = null;
				output = response.getOutputStream();
				Files.copy(path, output);

			} catch (IOException e) {

				String uuid = Common.geraUUID();
				errorEntity.setUuid(uuid);
				errorEntity.setDetail(e.getMessage().toString());
				errorEntity.setDateEntry(Common.hoje());
				errorEntity.setHourEntry(Common.hora());
				errorEntity.setUserEntity(userEntity);
				daoError.saveUpdate(errorEntity);

				

			}

		}

	}

	@RequestMapping(value = { "/anexosdelete/{id}/{tipo}/{tipoID}" }, method = RequestMethod.GET)
	public ModelAndView anexodelete(@PathVariable(value = "id") String id, @PathVariable(value = "tipo") String tipo,
			@PathVariable(value = "tipoID") String tipoID, HttpServletRequest request, HttpServletResponse response,
			ModelAndView model) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoDocument.exist(DocumentEntity.class, map, "and")) {
			documentEntity = daoDocument.findByProperty(DocumentEntity.class, map, "and");

			Common.documentoDelete(documentEntity);
			daoDocument.delete(DocumentEntity.class, map, "and");

		}
		if (tipo.equals("passaporte")) {
			model.setViewName("redirect:/passaporte/ver/" + tipoID);
		}
		if (tipo.equals("afastamento")) {
			model.setViewName("redirect:/afastamento/edit/" + tipoID);
		}

		if (tipo.equals("pessoal")) {
			model.setViewName("redirect:/user/edit/" + tipoID);
		}
		if (tipo.equals("cmc")) {
			model.setViewName("redirect:/cmc/ver/" + tipoID);
		}
		return model;

	}
}
