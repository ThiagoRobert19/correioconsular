package com.trsystem.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.DocumentAtosEntity;
import com.trsystem.model.DocumentStatusEntity;
import com.trsystem.model.ErrorEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;
import com.trsystem.util.Common;
import com.trsystem.util.Criptografia;

@Controller
@RequestMapping(value = "/documentatos")
public class DocumentAtosController {
	@Autowired
	private UserEntity userEntity;
	@Autowired
	private GenericDao<DocumentAtosEntity> daoDocumentAtos;
	@Autowired
	private List<DocumentAtosEntity> list;
	@Autowired
	private DocumentAtosEntity documentAtosEntity;

	@Autowired
	private ErrorEntity errorEntity;
	@Autowired
	private GenericDao<ErrorEntity> daoError;

	@RequestMapping(value = { "/data" }, method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, ModelAndView model) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");
			return model;

		}
		list=daoDocumentAtos.list(DocumentAtosEntity.class);

		model.addObject("list", list);
		model.setViewName("documentatos/data");
		return model;

	}

	@RequestMapping(value = { "/add" }, method = RequestMethod.GET)
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {
		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}
		model.setViewName("documentatos/add");

		return model;

	}

	@RequestMapping(value = "/saveupdate", method = RequestMethod.POST)
	public ModelAndView saveupdate(DocumentAtosEntity documentAtos, String valor, HttpServletRequest request,
			ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", documentAtos.getName());
		if (documentAtos.getId() == null || documentAtos.getId().equals(null)) {

			if (daoDocumentAtos.exist(DocumentAtosEntity.class, map, "and")) {

				model.addObject("erro", " N�o � poss�vel adicionar o Documento Atos, ja Existe Tipo de Documento com o mesmo nome!");

				model.setViewName("documentatos/add");
				return model;
			}
			
		}
		documentAtos.setValorDocumento(new BigDecimal(valor));
		daoDocumentAtos.saveUpdate(documentAtos);

		model.setViewName("redirect:/documentatos/data");

		return model;

	}

	@RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		if (!userEntity.getTipo().equals("Administrador")) {
			model.setViewName("redirect:/");

			return model;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoDocumentAtos.exist(DocumentAtosEntity.class, map, "and")) {
			documentAtosEntity = daoDocumentAtos.findByProperty(DocumentAtosEntity.class, map, "and");
			model.addObject("documentAtosEntity", documentAtosEntity);
			model.setViewName("/documentatos/edit");
			return model;
		} else {
			model.setViewName("redirect:/documentatos/data");
			return model;
		}

	}

}
