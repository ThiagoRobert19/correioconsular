package com.trsystem.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class AutorizacaoInterceptor extends HandlerInterceptorAdapter {
	

	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller)
			throws Exception {

		String uri = request.getRequestURI();
		if (uri.contains("resources") || uri.endsWith("/")||(uri.endsWith("/login/logout"))||(uri.endsWith("/login/simple")||uri.endsWith("/login/simple1")||uri.endsWith("/login/simpleextra")|| (request.getSession().getAttribute("clienteLogado") != null))) {
			return true;
		}
		
		response.sendRedirect(request.getContextPath() + "/");

		return false;
	}


}