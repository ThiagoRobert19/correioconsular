package com.trsystem.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.AfastamentoEntity;
import com.trsystem.model.CarteiraEntity;
import com.trsystem.model.DocumentEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;

public class Common {
	static SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
	static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	static SimpleDateFormat formatmesdiaano = new SimpleDateFormat("MM/dd/yy");
	static SimpleDateFormat formatHojeHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	static SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");

	public static Date passaDate(String passar) {

		Date data = new Date();
		Date passa = new Date();
		try {
			data = formato.parse(passar);
			String b = format.format(data);
			passa = format.parse(b);
		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}
	public static String formataCalendarDe(String passar) {

		Date data = new Date();
		
		String passa = "";
		try {
			data = formato.parse(passar);
			
			passa = formato.format(data);

		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}
	public static String formataCalendarAte(String passar) {

		Date data = new Date();
		
		String passa = "";
		try {
			data = formato.parse(passar);
			
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(data); 
			c.add(Calendar.DATE, 1);
			data = c.getTime();
			
			passa = formato.format(data);

		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}
	public static Date passamesdiaano(String passar) {

		Date data = new Date();
		Date passa = new Date();
		try {
			data = formatmesdiaano.parse(passar);
			
			
			String b = format.format(data);
			passa = format.parse(b);
		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}
	public static Date formata(Date hoje) {
		Date passa = new Date();

		

		try {
			String b = format.format(hoje);
			passa = format.parse(b);
		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}
	public static Date hoje() {
		Date passa = new Date();

		Date data = new Date();

		try {
			String b = format.format(data);
			passa = format.parse(b);
		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}

	public static Date hojeHora() {
		Date passa = new Date();

		Date data = new Date();

		try {
			String b = formatHojeHora.format(data);
			passa = formatHojeHora.parse(b);
		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}
	public static Date hora() {
		Date passa = new Date();

		Date data = new Date();

		try {
			String b = formatHora.format(data);
			passa = formatHora.parse(b);
		} catch (ParseException e) {
			System.out.println("Erro: " + e.toString());
			e.printStackTrace();
		}

		return passa;
	}

	public static String geraSenha() {
		String[] carct = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

		String senha = "NOVA";

		for (int x = 0; x < 5; x++) {
			int j = (int) (Math.random() * carct.length);
			senha += carct[j];
		}

		return senha;
	}

	public static String geraUUID() {
		UUID gfg1 = UUID.randomUUID();

		return gfg1.toString();
	}

	public static void documentoPassaporte(PassaporteEntity passaporte, MultipartFile file, DocumentEntity documento,
			GenericDao<DocumentEntity> daoDocument) {
		Calendar cal = Calendar.getInstance();

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;

		String pasta = geraSenha();

		File dir = new File("C:/arquivos/" + year + "/" + month + "/" + passaporte.getProtocol() + pasta);
		if (!dir.exists()) {

			new File("C:/arquivos/" + year + "/" + month + "/" + passaporte.getProtocol() + pasta).mkdirs();
		}
		try {

			String extensao = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

			String nome = file.getOriginalFilename();

			String local = "C:/arquivos/" + year + "/" + month + "/" + passaporte.getProtocol() + pasta + "/" + nome
					+ extensao;

			File convFile = new File(local);

			file.transferTo(convFile);
			documento.setNomeDocumento(nome.trim() + extensao);
			documento.setLocalDocumento(local);
			documento.setPassaporteEntity(passaporte);
			daoDocument.saveUpdate(documento);

		} catch (IllegalStateException | IOException e) {
			System.out.println("erro: " + e.getMessage().toString());
			e.printStackTrace();
		}
	}
	public static void documentoCarteira(CarteiraEntity carteira, MultipartFile file, DocumentEntity documento,
			GenericDao<DocumentEntity> daoDocument) {
		Calendar cal = Calendar.getInstance();

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;

		String pasta = geraSenha();

		File dir = new File("C:/arquivos/" + year + "/" + month + "/" + carteira.getProtocol() + pasta);
		if (!dir.exists()) {

			new File("C:/arquivos/" + year + "/" + month + "/" + carteira.getProtocol() + pasta).mkdirs();
		}
		try {

			String extensao = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

			String nome = file.getOriginalFilename();

			String local = "C:/arquivos/" + year + "/" + month + "/" + carteira.getProtocol() + pasta + "/" + nome
					+ extensao;

			File convFile = new File(local);

			file.transferTo(convFile);
			documento.setNomeDocumento(nome.trim() + extensao);
			documento.setLocalDocumento(local);
			documento.setCarteiraEntity(carteira);
			daoDocument.saveUpdate(documento);

		} catch (IllegalStateException | IOException e) {
			System.out.println("erro: " + e.getMessage().toString());
			e.printStackTrace();
		}
	}
	public static void documentoDelete(DocumentEntity documento) {

		try {

			File dir = new File(documento.getLocalDocumento());
			dir.delete();

			

		} catch (IllegalStateException e) {
			System.out.println("erro: " + e.getMessage().toString());
			e.printStackTrace();
		}
	}
	public static void documentoAfastamento(UserEntity userEntity, MultipartFile file, AfastamentoEntity afastamentoEntity) {
		
		DocumentEntity documentEntity = new DocumentEntity();
		GenericDao<DocumentEntity> daoDocument = new GenericDao<DocumentEntity>();

		Calendar cal = Calendar.getInstance();

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;

		String pasta = geraSenha();

		File dir = new File("C:/arquivos/afastamento/" + year + "/" + month + "/" + userEntity.getName() + pasta);
		if (!dir.exists()) {

			new File("C:/arquivos/afastamento/" + year + "/" + month + "/" + userEntity.getName() + pasta).mkdirs();
		}
		try {

			String extensao = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

			String nome = file.getOriginalFilename();

			String local = "C:/arquivos/afastamento/" + year + "/" + month + "/" + userEntity.getName() + pasta + "/" + nome
					+ extensao;

			File convFile = new File(local);

			file.transferTo(convFile);
			documentEntity.setNomeDocumento(nome.trim() + extensao);
			documentEntity.setLocalDocumento(local);
			documentEntity.setAfastamentoEntity(afastamentoEntity);
			
			daoDocument.saveUpdate(documentEntity);

		} catch (IllegalStateException | IOException e) {
			System.out.println("erro: " + e.getMessage().toString());
			e.printStackTrace();
		}
	}
	

}
