package com.trsystem.util;

import java.io.File;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;

import jxl.NumberCell;

import jxl.Sheet;

import jxl.Workbook;

import jxl.read.biff.BiffException;

import javax.swing.JOptionPane;

import com.trsystem.dao.GenericDao;
import com.trsystem.model.DocumentStatusEntity;
import com.trsystem.model.ObservationHistoryEntity;
import com.trsystem.model.PassaporteEntity;
import com.trsystem.model.UserEntity;

public class ImportExcellData {

	public void abreplanilha() throws IOException, BiffException {
		Workbook workbook = Workbook.getWorkbook(new File("C:/excel/Passaportes.xls"));
		Sheet sheet = workbook.getSheet(0);

		int linhas = sheet.getRows();
		System.out.println("Quantidade de linhas= " + linhas);
		System.out.println("Iniciando a leitura da planilha XLS:");

		for (int i = 0; i < linhas; i++) {

			Cell protocolo = sheet.getCell(1, i);

			Cell dateEntry = sheet.getCell(2, i);

			Cell userEntity = sheet.getCell(3, i);

			Cell personName = sheet.getCell(4, i);

			Cell status = sheet.getCell(5, i);

			Cell dateDeparture = sheet.getCell(6, i);

			Cell observacao = sheet.getCell(7, i);

			Cell personPhone = sheet.getCell(8, i);

			Cell personEmail = sheet.getCell(9, i);

			String as1 = protocolo.getContents();

			String as2 = dateEntry.getContents();

			String as3 = userEntity.getContents();

			String as4 = personName.getContents();

			String as5 = status.getContents();

			String as6 = dateDeparture.getContents();

			String as7 = observacao.getContents();

			String as8 = personPhone.getContents();

			String as9 = personEmail.getContents();

			System.out.println("Coluna 1: " + as1);

			System.out.println("Coluna 2: " + as2);

			System.out.println("Coluna 3: " + as3);

			System.out.println("Coluna 4: " + as4);

			System.out.println("Coluna 5: " + as5);

			System.out.println("Coluna 6: " + as6);
			System.out.println("Coluna 7: " + as7);
			System.out.println("Coluna 8: " + as8);

			System.out.println("Coluna 9: " + as9);
		}
		workbook.close();
	}

	public void convertePassaporte(GenericDao<ObservationHistoryEntity> daoObservation,GenericDao<PassaporteEntity> daoPassaporte,GenericDao<DocumentStatusEntity> daoDocumentStatus,GenericDao<UserEntity> daoUser) throws IOException, BiffException {

		
		ObservationHistoryEntity observationEntity = new ObservationHistoryEntity();
		PassaporteEntity passaporteEntity = new PassaporteEntity();
		PassaporteEntity passaporte = new PassaporteEntity();
		UserEntity userEntity = new UserEntity();
		DocumentStatusEntity documentStatusEntity = new DocumentStatusEntity();
	

		Workbook workbook = Workbook.getWorkbook(new File("C:/excel/Passaportes.xls"));
		Sheet sheet = workbook.getSheet(0);

		int linhas = sheet.getRows();

		System.out.println("Quantidade de linhas= " + linhas);
		System.out.println("Iniciando a leitura da planilha XLS:");

		for (int i = 1; i < linhas; i++) {

			Cell protocol = sheet.getCell(1, i);

			Cell dateEntry = sheet.getCell(2, i);

			Cell user = sheet.getCell(3, i);

			Cell personName = sheet.getCell(4, i);

			Cell status = sheet.getCell(5, i);

			Cell dateDeparture = sheet.getCell(6, i);

			Cell observacao = sheet.getCell(7, i);

			Cell personPhone = sheet.getCell(8, i);

			Cell personEmail = sheet.getCell(9, i);

			int observaresposta = 0;
			// ==================================
			if (!user.getContents().equals("") && user.getContents() != null) {
				observaresposta = 1;
				Map<String, Object> mapuser = new HashMap<String, Object>();
				int idUser = Integer.parseInt(user.getContents());
				switch (idUser) {

				case 2:
					mapuser.put("email", "cristina.martins@itamaraty.gov.br");
					break;
				case 3:
					mapuser.put("email", "tarcisio.vieira@itamaraty.gov.br");
					break;
				case 4:
					mapuser.put("email", "thais.lopes@itamaraty.gov.br");
					break;
				case 5:
					mapuser.put("email", "francisco.serra@itamaraty.gov.br");
					break;
				case 6:
					mapuser.put("email", "maria.santa@itamaraty.gov.br");
					break;
				case 7:
					mapuser.put("email", "ieda.rodrigues@itamaraty.gov.br");
					break;
				case 8:
					mapuser.put("email", "ana.lucia@itamaraty.gov.br");
					break;
				case 9:
					mapuser.put("email", "erica.zimmerman@itamaraty.gov.br");
					break;
				case 10:
					mapuser.put("email", "erika.roma@itamaraty.gov.br");
					break;
				case 11:
					mapuser.put("email", "marcia.ribeiro@itamaraty.gov.br");
					break;
				case 12:
					mapuser.put("email", "marilia.silva@itamaraty.gov.br");
					break;
				case 13:
					mapuser.put("email", "marinalda.jones@itamaraty.gov.br");
					break;
				case 14:
					mapuser.put("email", "mery.rodrigues@itamaraty.gov.br");
					break;
				case 15:
					mapuser.put("email", "patricia.francisco@itamaraty.gov.br");
					break;
				case 16:
					mapuser.put("email", "renato.saraiva@itamaraty.gov.br");
					break;
				case 17:
					mapuser.put("email", "sintia.tomazzi@itamaraty.gov.br");
					break;
				case 18:
					mapuser.put("email", "thais.lima@itamaraty.gov.br");
					break;
				case 19:
					mapuser.put("email", "valeria.martelli@itamaraty.gov.br");
					break;
				case 20:
					mapuser.put("email", "bruno.slomp@itamaraty.gov.br");
					break;
				case 21:
					mapuser.put("email", "cecilia.vieira@itamaraty.gov.br");
					break;
				case 22:
					mapuser.put("email", "fernanda.klusener@itamaraty.gov.br");
					break;
				case 23:
					mapuser.put("email", "alyssa.baracat@itamaraty.gov.br");
					break;
				case 24:
					mapuser.put("email", "paulo.paiani@itamaraty.gov.br");
					break;
				case 25:
					mapuser.put("email", "diane.macedo@itamaraty.gov.br");
					break;
				case 26:
					mapuser.put("email", "fernanda.borin@itamaraty.gov.br");
					break;
				case 27:
					mapuser.put("email", "ligia.trad@itamaraty.gov.br");
					break;
				case 28:
					mapuser.put("email", "heloisa.gardner@itamaraty.gov.br");
					break;
				case 29:
					mapuser.put("email", "otavio.cordioli@itamaraty.gov.br");
					break;
				}
				userEntity = daoUser.findByProperty(UserEntity.class, mapuser, "and");
				passaporteEntity.setAtendente(userEntity);
			}

			// ============================================================

			if (!status.getContents().equals("") && status.getContents() != null) {
				Map<String, Object> mapstatus = new HashMap<String, Object>();

				int idStatus = Integer.parseInt(status.getContents());
				switch (idStatus) {

				case 1:
					mapstatus.put("id", 34);
					break;
				case 4:
					mapstatus.put("id", 35);
					break;
				case 5:
					mapstatus.put("id", 36);
					break;
				case 6:
					mapstatus.put("id", 37);
					break;
				case 7:
					mapstatus.put("id", 38);
					break;
				case 9:
					mapstatus.put("id", 39);
					break;
				case 10:
					mapstatus.put("id", 79);
					break;
				}
				documentStatusEntity = daoDocumentStatus.findByProperty(DocumentStatusEntity.class, mapstatus, "and");
				passaporteEntity.setDocumentStatus(documentStatusEntity);
			}

			// ==================================
			passaporteEntity.setProtocol(protocol.getContents().trim());

			if (dateEntry.getContents() != null && !dateEntry.getContents().equals("")) {
				Date passa2 = Common.passamesdiaano(dateEntry.getContents());
				passaporteEntity.setDateEntry(passa2);
			}
			if (dateDeparture.getContents() != null && !dateDeparture.getContents().equals("")) {
				Date passa2 = Common.passamesdiaano(dateDeparture.getContents());
				passaporteEntity.setDateDeparture(passa2);
			}
			passaporteEntity.setPersonName(personName.getContents().trim());
			passaporteEntity.setPersonEmail(personEmail.getContents());
			passaporteEntity.setPersonPhone(personPhone.getContents());

			daoPassaporte.saveUpdate(passaporteEntity);

			Map<String, Object> mapprotocolo = new HashMap<String, Object>();
			mapprotocolo.put("protocol", protocol.getContents().trim());

			passaporte = daoPassaporte.findByProperty(PassaporteEntity.class, mapprotocolo, "and");
			if (observaresposta == 1) {
				if (observacao.getContents().trim() != null && !observacao.getContents().trim().equals("")) {
					observationEntity.setDateObervation(Common.hojeHora());
					observationEntity.setObservation(observacao.getContents().trim());
					observationEntity.setPassaporteEntity(passaporte);

					observationEntity.setTipoDocumento("passaporte");
					observationEntity.setUserEntity(userEntity);

					SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					String stringDate = DateFor.format(observationEntity.getDateObervation());

					observationEntity.setDetail("Data e Hora: " + stringDate + "| Atendente: " + userEntity.getName()
							+ "| Obs: " + observacao.getContents().trim());

					daoObservation.saveUpdate(observationEntity);

				}
			}

		}
		workbook.close();
	}
}
