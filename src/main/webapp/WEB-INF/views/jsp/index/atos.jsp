<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="row">
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-info shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<c:if test="${clienteLogado.acessoAtos == 'Sim' || clienteLogado.email == 'Administrador'}">
							<a class="text-xs font-weight-bold text-info text-uppercase mb-1"
								href="<c:url value='/atos/quando/semana'/>">Atos Notarial
								Inseridos na Semana</a>
						</c:if>
						<c:if test="${clienteLogado.acessoAtos != 'Sim' && clienteLogado.email != 'Administrador'}">
							<a class="text-xs font-weight-bold text-info text-uppercase mb-1"
								href="#>">Atos Notarial Inseridos na Semana</a>
						</c:if>
						<div class="row no-gutters align-items-center">
							<div class="col-auto">
								<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">${countatossemana}</div>
							</div>

						</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-calendar fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<c:if test="${clienteLogado.acessoAtos == 'Sim' || clienteLogado.email == 'Administrador'}">
							<a class="text-xs font-weight-bold text-primary text-uppercase mb-1"
								href="<c:url value='/atos/quando/mes'/>">Atos Notarial
								Inseridos no M�s</a>
						</c:if>
						<c:if test="${clienteLogado.acessoAtos != 'Sim' && clienteLogado.email != 'Administrador'}">
							<a class="text-xs font-weight-bold text-primary text-uppercase mb-1"
								href="#">Atos Notarial Inseridos no M�s</a>
						</c:if>


						<div class="h5 mb-0 font-weight-bold text-gray-800">${countatosmes}</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-calendar fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<c:if test="${clienteLogado.acessoAtos == 'Sim' || clienteLogado.email == 'Administrador'}">
							<a class="text-xs font-weight-bold text-success text-uppercase mb-1"
								href="<c:url value='/atos/quando/ano'/>">Atos Notarial
								Inseridos No Ano</a>
						</c:if>
						<c:if test="${clienteLogado.acessoAtos != 'Sim' && clienteLogado.email != 'Administrador'}">
							<a class="text-xs font-weight-bold text-success text-uppercase mb-1"
								href="#">Atos Notarial Inseridos No Ano</a>
						</c:if>


						<div class="h5 mb-0 font-weight-bold text-gray-800">${countatosano}</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-calendar fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Earnings (Monthly) Card Example -->


	<!-- Pending Requests Card Example -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-warning shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<a
							class="text-xs font-weight-bold text-warning text-uppercase mb-1"
							href="#">Atos Notarial Inseridos Total</a>
						<div class="h5 mb-0 font-weight-bold text-gray-800">${countatostotal}</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-calendar fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>