<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<c:if test="${empty clienteLogado.email}">
	<c:import url="/WEB-INF/views/jsp/index/login.jsp" />
</c:if>
<c:if test="${not empty clienteLogado.email}">
	<c:import url="/WEB-INF/views/jsp/index/dashboard.jsp" />
</c:if>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
