<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="row justify-content-center">

	<div class="col-xl-6 col-lg-6 col-md-9">

		<div class="card o-hidden border-0 shadow-lg my-5">
			<div class="card-body p-0">
				<!-- Nested Row within Card Body -->
				<div class="row">
					<div class="col-lg-3 d-none d-lg-block "></div>
					<div class="col-lg-6">
						<div class="p-5">
							<div class="text-center">
								<h1 class="h4 text-gray-900 mb-4">Welcome Administrador!</h1>
							</div>
							<form class="user"
								action="<c:url value='/login/simpleextra'/>"
								method="post">
								<div class="form-group">
									<input type="text" class="form-control form-control-user"
										
										placeholder="Enter Email Address..." name="email">
								</div>
								<div class="form-group">
									<input type="password" class="form-control form-control-user"
										id="exampleInputPassword" placeholder="Password"
										name="password">
								</div>
								<!--  div class="form-group">
									<div class="custom-control custom-checkbox small">
										<input type="checkbox" class="custom-control-input"
											id="customCheck"> <label class="custom-control-label"
											for="customCheck">Remember Me</label>
									</div>
								</div-->

								<button type="submit" class="btn btn-primary btn-user btn-block">
									<i class="fa fa-dot-circle-o"></i> Login
								</button>

								<!--a href="index.html" class="btn btn-google btn-user btn-block">
									<i class="fab fa-google fa-fw"></i> Login with Google
								</a> <a href="index.html"
									class="btn btn-facebook btn-user btn-block"> <i
									class="fab fa-facebook-f fa-fw"></i> Login with Facebook
								</a-->
							</form>


							<!-- div class="text-center">
								<a class="small" href="forgot-password.html">Forgot
									Password?</a>
							</div-->

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

