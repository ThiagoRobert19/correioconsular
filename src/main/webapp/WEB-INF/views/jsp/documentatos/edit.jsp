<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Editar Status ${documentAtosEntity.name}</h1>
						</div>
						<form class="user" action="<c:url value='/documentatos/saveupdate'/>"
							method="post" enctype="multipart/form-data">
							<input type="hidden" name="id" value="${documentAtosEntity.id}">
							
							
							<div class="form-group row">
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Nome*</span> <input type="text" class="form-control" value="${documentAtosEntity.name}"
										name="name" required="required">
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Detalhes</span> <input type="text" class="form-control" value="${documentAtosEntity.detail}"
										name="detail" required="required">
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Valor*</span> <input type="text" class="form-control" value="${documentAtosEntity.valorDocumento}"
										name="valor" required="required">
								</div>
								
							</div>
							
							<div class="form-group row">
								<div class="col-sm-2">
									<button type="submit" class="btn btn-primary btn-user btn-block">
										<i class="fa fa-dot-circle-o"></i> Editar
									</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
