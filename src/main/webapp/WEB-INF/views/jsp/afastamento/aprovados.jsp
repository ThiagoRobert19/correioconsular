<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">Lista de Afastamentos Aprovados</h1>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Afastamentos</h6>
		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered sortable" id="dataTable"
					width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Requerente</th>
							<th>Substituto</th>
							<th>De</th>
							<th>At�</th>
							<th>Tipo</th>
							<th>Status</th>
							<th>Op��o</th>

						</tr>
					</thead>

					<tbody>
						<c:forEach var="list" items="${list}">
							<tr>
								<td>${list.requester.name}</td>
								<td>${list.substitute.name}</td>
								<td>${list.dateStart}</td>
								<td>${list.dateEnd}</td>
								<td>${list.tipo}</td>
								<td>${list.status}</td>
								<td><a
									href="<c:url value='/afastamento/revoke/${list.id}'/>"
									class="btn btn-danger btn-sm"> Cancelar</a> <a
									href="<c:url value='/afastamento/editmanual/${list.id}'/>"
									class="btn btn-warning btn-sm"> Alterar</a></td>

							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>




<c:import url="/WEB-INF/cabecalho/footer.jsp" />
