<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">



	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->

			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">

						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Novo Afastamento para
								${userEntity.name}!</h1>
						</div>
						<form class="user"
							action="<c:url value='/afastamento/requerer'/>" method="post"
							enctype="multipart/form-data">
							<input type="hidden" name="requesterID" value="${userEntity.id}" />
							<div class="form-group row">
								<div class="col-sm-4">
									<span>Requerente:</span> <input type="text" class="form-control"
										value="${userEntity.name}" required="required"
										readonly="readonly" />
								</div>
								<div class="col-sm-4">
									<span>Substituto:</span> <select class="form-control"
										name="substituteID">
										<c:forEach var="listSubstitute" items="${listSubstitute}">
											<option value="${listSubstitute.id}">${listSubstitute.name}</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4">
									<span>Arquivos (Tamanho m�ximo 20 mb)</span> <input type="file"
										name="file" placeholder="Choose a file" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-4">
									<span>Tipo de Afastamento:</span> <select class="form-control"
										name="tipo">

										<option value="Ferias">Ferias</option>
										<option value="Sick Leave">Sick Leave</option>
										<option value="Medical Leave">Medical Leave</option>
										<option value="Maternidade">Maternidade</option>
										<option value="Paternidade">Paternidade</option>
										<option value="Nojo">Nojo</option>
										<option value="Juri">Juri</option>


									</select>
								</div>
								<div class="col-sm-4">
									<span>De:</span> <input type="date" name="de"
										class="form-control" id="txtDateDe">
								</div>
								<div class="col-sm-4">
									<span>At�:</span> <input type="date" name="ate"
										class="form-control" id="txtDateAte">
								</div>

							</div>

							<div class="form-group row">
								<div class="col-sm-2">
									<button type="submit"
										class="btn btn-primary btn-user btn-block">
										<i class="fa fa-dot-circle-o"></i> Criar

									</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>




</div>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var calendarEl = document.getElementById('calendar');

		var calendar = new FullCalendar.Calendar(calendarEl, {
			selectable : true,

			headerToolbar : {
				left : 'prev,next',
				center : 'title',
				right : ''
			},
			displayEventTime : false, // don't show the time column in list view

			events : [ {
				title : 'event1',
				start : '2020-09-09'
			}, {
				title : 'event2',
				start : '2020-09-09',
				end : '2020-09-10'
			}, {
				title : 'event3',
				start : '2020-09-09T12:30:00',
				allDay : false
			// will make the time show
			} ]

		});

		calendar.render();
	});
</script>
<script>
	$(function() {
		var dtToday = new Date();

		var month = dtToday.getMonth() + 1;
		var day = dtToday.getDate();
		var year = dtToday.getFullYear();
		if (month < 10)
			month = '0' + month.toString();
		if (day < 10)
			day = '0' + day.toString();

		var maxDate = year + '-' + month + '-' + day;

		$('#txtDateDeas').attr('min', maxDate);
		$('#txtDateAteas').attr('min', maxDate);
	});
</script>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
