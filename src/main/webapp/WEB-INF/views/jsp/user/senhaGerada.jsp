<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	
	

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Senha Gerada para o Fucionário ${userEntity.name}</h6>
			<h6 class="m-0 font-weight-bold text-primary"> ${senhaGerada}</h6>
		</div>

	</div>

</div>




<c:import url="/WEB-INF/cabecalho/footer.jsp" />
