<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">Lista de Funcion�rios com acesso
		ao sistema do Setor Consular</h1>
	<p class="mb-4">
		Aqui voc� pode gerenciar todos os Usuarios deste sistema. <a
			href="<c:url value='/user/permit'/>">Clique aqui</a> para dar acesso
		a um novo Funcion�rio.
	</p>


	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Funcion�rio</h6>
		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered sortable" id="dataTable"
					width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Tipo</th>
							<th>Passaporte</th>
							<th>Atos</th>
							<th>CMC</th>
							<th>Op��o</th>

						</tr>
					</thead>

					<tbody>
						<c:forEach var="listAcesso" items="${listAcesso}">
							<tr>
								<td>${listAcesso.userEntity.name}</td>
								<td>${listAcesso.tipo}</td>
								<td>${listAcesso.passaporte}</td>
								<td>${listAcesso.atosNotariais}</td>
								<td>${listAcesso.cmc}</td>
								<td><a
									href="<c:url value='/user/revoke/${listAcesso.id}'/>"
									class="btn btn-danger btn-sm"> Revogar Acesso </a> <a
									href="<c:url value='/user/changeaccess/${listAcesso.id}'/>"
									class="btn btn-warning btn-sm"> Alterar Acesso </a></td>

							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>




<c:import url="/WEB-INF/cabecalho/footer.jsp" />
