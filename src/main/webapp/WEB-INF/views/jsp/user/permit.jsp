<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Permissão ao Funcionário</h1>
						</div>
						<form class="user" action="<c:url value='/user/givepermit'/>"
							method="post" enctype="multipart/form-data">



							<div class="form-group row">
								<div class="col-sm-6 mb-3 mb-sm-0">
									<span>Funcionario*</span> <select name="userID"
										class="form-control">
										<c:forEach var="listExtra" items="${listExtra}">
											<option value="${listExtra.id}">${listExtra.name}</option>

										</c:forEach>

									</select>
								</div>
								<div class="col-sm-6">
									<span>Tipo</span> <select name="tipo" class="form-control">
										<option value="Administrador">Administrador</option>
										<option value="Atendente">Atendente</option>

									</select>
								</div>

							</div>
							<div class="form-group row">
								<div class="col-sm-4">
									<span>Acesso Passaporte?*</span>
									<select name="acessoPassaporte" class="form-control">
									
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>
								<div class="col-sm-4">
									<span>Acesso Atos Notariais?*</span>
									<select name="acessoAtos" class="form-control">
									
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>
								<div class="col-sm-4">
									<span>Acesso CMC?*</span>
									<select name="acessoCMC" class="form-control">
									
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>

							</div>
							<div class="form-group row">
								<div class="col-sm-2">
									<button type="submit"
										class="btn btn-primary btn-user btn-block">
										<i class="fa fa-dot-circle-o"></i> Conceder
									</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
