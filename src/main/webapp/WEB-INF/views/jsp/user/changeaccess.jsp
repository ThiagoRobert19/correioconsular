<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Alterar Permissão ao
								Funcionário ${acessoEntity.userEntity.name}</h1>
						</div>
						<form class="user" action="<c:url value='/user/changepermit'/>"
							method="post" enctype="multipart/form-data">

							<input type="hidden" name="acessoID" value="${acessoEntity.id}" />

							<div class="form-group row">
								<div class="col-sm-4">
									<span>Acesso Atual*</span> <input type="text" readonly="readonly" class="form-control" value="${acessoEntity.tipo}" />
								</div>
								<div class="col-sm-4">
									<span>Alterar Tipo de acesso?*</span>
									<select name="alteratipo" class="form-control">
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>
							
								<div class="col-sm-4">
									<span>Acesso*</span> <select name="tipoNovo" class="form-control">
										<option value="${acessoEntity.tipo}">${acessoEntity.tipo}</option>
										<option value="Administrador">Administrador</option>
										<option value="Atendente">Atendente</option>
									</select>
								</div>


							</div>
							<div class="form-group row">
								<div class="col-sm-4">
									<span>Acesso Passaporte*</span> <input type="text" readonly="readonly" class="form-control" value="${acessoEntity.passaporte}" />
								</div>
								<div class="col-sm-4">
									<span>Alterar  acesso Passaporte?*</span>
									<select name="alterapassaporte" class="form-control">
									
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>
							
								<div class="col-sm-4">
									<span>Acesso Passaporte*</span> <select name="tipoPassaporte" class="form-control">
										<c:if test="${acessoEntity.passaporte=='Sim'}"><option value="Sim">Conceder</option></c:if>
										<c:if test="${acessoEntity.passaporte=='Nao'}"><option value="Nao">Revogar</option></c:if>
										<option value="Sim">Conceder</option>
										<option value="Nao">Revogar</option>
									</select>
								</div>


							</div>
							<div class="form-group row">
								<div class="col-sm-4">
									<span>Acesso Atos*</span> <input type="text" readonly="readonly" class="form-control" value="${acessoEntity.atosNotariais}" />
								</div>
								<div class="col-sm-4">
									<span>Alterar  acesso Atos Notariais?*</span>
									<select name="alteraatos" class="form-control">
									
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>
							
								<div class="col-sm-4">
									<span>Acesso Atos Notariais*</span> <select name="tipoAtos" class="form-control">
										<c:if test="${acessoEntity.atosNotariais=='Sim'}"><option value="Sim">Conceder</option></c:if>
										<c:if test="${acessoEntity.atosNotariais=='Nao'}"><option value="Nao">Revogar</option></c:if>
										<option value="Sim">Conceder</option>
										<option value="Nao">Revogar</option>
									</select>
								</div>


							</div>
							<div class="form-group row">
								<div class="col-sm-4">
									<span>Acesso CMC*</span> <input type="text" readonly="readonly" class="form-control" value="${acessoEntity.cmc}" />
								</div>
								<div class="col-sm-4">
									<span>Alterar  acesso CMC?*</span>
									<select name="alteracmc" class="form-control">
									
										<option value="Sim">Sim</option>
										<option value="Nao">Nao</option>
									</select>
								</div>
							
								<div class="col-sm-4">
									<span>Acesso CMC*</span> <select name="tipoCMC" class="form-control">
										<c:if test="${acessoEntity.cmc=='Sim'}"><option value="Sim">Conceder</option></c:if>
										<c:if test="${acessoEntity.cmc=='Nao'}"><option value="Nao">Revogar</option></c:if>
										<option value="Sim">Conceder</option>
										<option value="Nao">Revogar</option>
									</select>
								</div>


							</div>
							<div class="form-group row">
								<div class="col-sm-2">
									<button type="submit"
										class="btn btn-primary btn-user btn-block">
										<i class="fa fa-dot-circle-o"></i> Alterar
									</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
