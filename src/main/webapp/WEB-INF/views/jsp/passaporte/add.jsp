<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->

			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">


						<div class="row">
							<form class="user" class="form-inline"
								action="<c:url value='/passaporte/getbyprotocol'/>"
								method="post" enctype="multipart/form-data">
								<div class="col-lg-12">
									<div class="input-group mb-4">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Protocolo</span>
										</div>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" name="protocolo">

										<div class="input-group-append">

											<button type="submit" class="btn btn-primary btn-sm">
												<i class="fa fa-dot-circle-o"></i> Search
											</button>
										</div>
									</div>

								</div>
							</form>
						</div>
						<br />

						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">

								<c:if test="${empty passaporteEntity.id}">Nova Entrada de
								Passaporte!</c:if>
								<c:if test="${not empty passaporteEntity.id}">
							Visualizando protocolo ${passaporteEntity.protocol}
							</c:if>
							</h1>
						</div>
						<form class="user"
							action="<c:url value='/passaporte/saveupdate'/>" method="post"
							enctype="multipart/form-data">
							<input type="hidden" name="id" value="${passaporteEntity.id}">
						
							<div class="form-group row">
								<div class="col-sm-3">
									<c:if test="${not empty passaporteEntity.id}">
										<span>Protocolo:</span>
										<input type="text" name="protocol"  class="form-control" required="required"
											 value="${passaporteEntity.protocol}">
									</c:if>
									<c:if test="${empty passaporteEntity.id}">
										<span>Protocolo:</span>
										<input type="text" name="protocol" class="form-control"
											id="fieldProtocolo" required="required"
											value="${passaporteEntity.protocol}">
									</c:if>
								</div>
								<div class="col-sm-5">
									<span>Interessado:</span> <input type="text"
										id="fieldInteressado" name="personName" class="form-control"
										required="required" value="${passaporteEntity.personName}">
								</div>
								<div class="col-sm-4">
									<span>Status:</span> <select name="statusID"
										class="form-control">
										<c:if test="${not empty passaporteEntity.id}">
											<option value="${passaporteEntity.documentStatus.id}">
												${passaporteEntity.documentStatus.name}</option>

										</c:if>

										<c:forEach var="listStatus" items="${listStatus}">
											<option value="${listStatus.id}">${listStatus.name}</option>

										</c:forEach>



									</select>
								</div>

							</div>


							<div class="form-group row">
								<div class="col-sm-3">
									<c:if test="${not empty passaporteEntity.id}">
										<span>Data de Entrada:</span>
										<c:if test="${clienteLogado.tipo=='Administrador'}">
											<input type="date" name="dataentrada" class="form-control"
												required="required" value="${passaporteEntity.dateEntry}">
										</c:if>
										<c:if test="${clienteLogado.tipo!='Administrador'}">
											<input type="date" name="dataentrada" class="form-control"
												required="required" readonly="readonly"
												value="${passaporteEntity.dateEntry}">
										</c:if>

									</c:if>
									<c:if test="${empty passaporteEntity.id}">
										<span>Data de Entrada:</span>
										<input type="date" name="dataentrada" class="form-control"
											required="required">
									</c:if>

								</div>

								<div class="col-sm-5">
									<c:if test="${not empty passaporteEntity.id}">
										<span>Atendente:</span>
										<input type="text" class="form-control" required="required"
											readonly="readonly"
											value="${passaporteEntity.atendente.name}">
									</c:if>
									<c:if test="${empty passaporteEntity.id}">
										<span>Atendente:</span>
										<input type="text" class="form-control" required="required"
											readonly="readonly" value="${userEntity.name}">
									</c:if>
								</div>

								<div class="col-sm-4">
									<span>Telefone:</span> <input type="text" name="personPhone"
										class="form-control" value="${passaporteEntity.personPhone}">
								</div>

							</div>
							<div class="form-group row">
								<div class="col-sm-4">
									<span>Endere�o:</span> <input type="text" name="personAddress"
										value="${passaporteEntity.personAddress}" class="form-control">
								</div>
								<div class="col-sm-4">
									<span>Email:</span> <input type="text" name="personEmail"
										value="${passaporteEntity.personEmail}" class="form-control">
								</div>
								<div class="col-sm-4">
									<span>Arquivos (Tamanho m�ximo 20 mb)</span> <input type="file"
										name="file" placeholder="Choose a file" class="form-control">
								</div>

							</div>
							<div class="form-group row">

								<div class="col-sm-3">
									<span>Tracking Number Envio:</span> <input type="text"
										id="fieldTracking"
										value="${passaporteEntity.trackingNumberEnvio}"
										name="trackingNumberEnvio" class="form-control">
								</div>
								<div class="col-sm-3">
									<span>Tracking Number Retorno:</span> <input type="text"
										id="fieldTrackingSaida"
										value="${passaporteEntity.trackingNumberRetorno}"
										name="trackingNumberRetorno" class="form-control">
								</div>

								<div class="col-sm-6">
									<span>Observa��o:</span> <input type="text" name="observa"
										class="form-control">


								</div>

							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<c:if test="${not empty passaporteEntity.id}">
										<span>Data de Saida:</span>
										<input type="date" name="datasaida" class="form-control"
											value="${passaporteEntity.dateDeparture}">
									</c:if>
									<c:if test="${empty passaporteEntity.id}">
										<span>Data de Saida:</span>
										<input type="date" name="datasaida" class="form-control">
									</c:if>

								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-2">
									<button type="submit" onclick="javascript: p=true;"
										class="btn btn-primary btn-user btn-block">
										<i class="fa fa-dot-circle-o"></i>
										<c:if test="${not empty passaporteEntity.id}">
								 Alterar
								</c:if>
										<c:if test="${ empty passaporteEntity.id}">
								 Criar
								</c:if>

									</button>
								</div>

							</div>
						</form>


						<div class="card shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary">Hist�rico de
									Altera��es:</h6>
							</div>

							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered sortable" width="100%"
										cellspacing="0">
										<thead>
											<tr>
												<th>Observa��o</th>
												<c:if test="${clienteLogado.tipo=='Administrador'}">
													<th>Op��o</th>
												</c:if>
											</tr>
										</thead>

										<tbody>
											<c:forEach var="listObservation" items="${listObservation}">
												<tr>
													<c:if test="${listObservation.detail ==''}">

													</c:if>

													<td>${listObservation.detail}</td>
													<c:if test="${clienteLogado.tipo=='Administrador'}">
														<td><a
															href="<c:url value='/passaporte/obsdelete/${listObservation.id}'/>"
															class="btn btn-danger btn-sm"> Deletar</a></td>
													</c:if>
												</tr>
											</c:forEach>

										</tbody>
									</table>
								</div>
							</div>
						</div>


						<div class="card shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary">Anexos</h6>
							</div>

							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered sortable" width="100%"
										cellspacing="0">
										<thead>
											<tr>
												<th>Anexo</th>
												<th>Op��o</th>
											</tr>
										</thead>

										<tbody>
											<c:forEach var="listDocument" items="${listDocument}">
												<tr>
													<td>${listDocument.nomeDocumento}</td>

													<td><a
														href="<c:url value='/document/anexos/${listDocument.id}'/>"
														class="btn btn-success btn-sm"> Baixar</a> <a
														href="<c:url value='/document/anexosdelete/${listDocument.id}/passaporte/${passaporteEntity.id}'/>"
														class="btn btn-danger btn-sm"> Deletar</a></td>

												</tr>
											</c:forEach>

										</tbody>
									</table>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script language="javascript">
	var p = false;
</script>

<script type="text/javascript">
	function forceInputUppercase(e) {
		var start = e.target.selectionStart;
		var end = e.target.selectionEnd;
		e.target.value = e.target.value.toUpperCase();
		e.target.setSelectionRange(start, end);
	}

	document.getElementById("fieldInteressado").addEventListener("keyup",
			forceInputUppercase, false);

	(function() {
		var fieldTracking = document.getElementById('fieldTracking');
		if (fieldTracking) {
			fieldTracking.addEventListener('keydown', function(mozEvent) {
				var event = window.event || mozEvent;
				if (event.keyCode === 13) {
					event.preventDefault();
				}
			});
		}
		if (fieldTrackingSaida) {
			fieldTrackingSaida.addEventListener('keydown', function(mozEvent) {
				var event = window.event || mozEvent;
				if (event.keyCode === 13) {
					event.preventDefault();
				}
			});
		}
		if (fieldProtocolo) {
			fieldProtocolo.addEventListener('keydown', function(mozEvent) {
				var event = window.event || mozEvent;
				if (event.keyCode === 13) {
					event.preventDefault();
				}
			});
		}
	})();
</script>

<c:import url="/WEB-INF/cabecalho/footer.jsp" />
