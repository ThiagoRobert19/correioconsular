<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">Lista de Status para Documentos</h1>
	<p class="mb-4">
		Aqui voc� pode gerenciar todos os Status. <a
			href="<c:url value='/documentstatus/add'/>">Clique aqui</a> para
		adicionar um novo Status.
	</p>

	<!-- DataTales Example -->



	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Status</h6>
		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered sortable" id="dataTable"
					width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Detalhes</th>

							<th>Op��o</th>

						</tr>
					</thead>

					<tbody>
						<c:forEach var="list" items="${list}">
							<tr>
								<td>${list.name}</td>
								<td>${list.detail}</td>

								<td><a
									href="<c:url value='/documentstatus/edit/${list.id}'/>"
									class="btn btn-primary btn-sm"> Editar </a></td>

							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>




<c:import url="/WEB-INF/cabecalho/footer.jsp" />
