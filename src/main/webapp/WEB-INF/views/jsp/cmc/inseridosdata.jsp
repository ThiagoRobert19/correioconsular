<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">Lista de Carteira de Matr�cula Consular inseridos ${textodata}</h1>


	<!-- DataTales Example -->


	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Carteira de Matr�cula Consular</h6>
		</div>
			
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered sortable" width="100%" id="dataTable"
					cellspacing="0">
					<thead>
						<tr>
							<th>Protocolo</th>
							<th>Interessado</th>
							<th>Email</th>
							<th>Telefone</th>
							<th>Status</th>
							<th>Atendente</th>
							<th>Op��o</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="list" items="${list}">
							<tr>
								<td>${list.protocol}</td>
								<td>${list.personName}</td>
								<td>${list.personEmail}</td>
								<td>${list.personPhone}</td>
								<td>${list.documentStatus.name}</td>
								<td>${list.atendente.name}</td>
								<td><a href="<c:url value='/cmc/ver/${list.id}'/>"
									class="btn btn-success btn-sm"> Ver</a> <c:if
										test="${clienteLogado.tipo=='Administrador'}">
										<a href="<c:url value='/cmc/delete/${list.id}'/>"
											class="btn btn-danger btn-sm"> Deletar</a>

									</c:if></td>
							</tr>
						</c:forEach>


					</tbody>

				</table>

			

			</div>

		</div>
	</div>

</div>







<c:import url="/WEB-INF/cabecalho/footer.jsp" />
