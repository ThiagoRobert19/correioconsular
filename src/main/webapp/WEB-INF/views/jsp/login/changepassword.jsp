<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Alterar senha!</h1>
						</div>
						<form class="user" action="<c:url value='/login/savepassword'/>"
							method="post" enctype="multipart/form-data">
							<input type="hidden" name="id" value="${employeeEntity.id}">


							<div class="form-group row">
								<div class="col-sm-12 mb-3 mb-sm-0">
									<span>Senha Atual*</span> <input type="password"
										class="form-control" name="senhaAtual" required="required">
								</div>


							</div>
							<div class="form-group row">

								<div class="col-sm-12">
									<span>Nova Senha</span> <input type="password"
										class="form-control" name="novaSenha" required="required">
								</div>


							</div>
							<div class="form-group row">

								<div class="col-sm-12">
									<span>Repita Nova Senha</span> <input type="password"
										class="form-control" name="novaRepetida" required="required">
								</div>

							</div>


							<button type="submit" class="btn btn-primary btn-user btn-block">
								<i class="fa fa-dot-circle-o"></i> Alterar
							</button>

						</form>


					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
<script>
	$('.add').on('click', function() {
		var options = $('select.multiselect1 option:selected').sort().clone();
		$('select.multiselect2').append(options);
		$('select.multiselect2').val($('select.multiselect1').val());

		$('select.multiselect1 option:selected').remove();
	});

	$('.remove').on('click', function() {
		var options = $('select.multiselect2 option:selected').sort().clone();
		$('select.multiselect1').append(options);
		$('select.multiselect1').val($('select.multiselect1').val());

		$('select.multiselect2 option:selected').remove();
	});
</script>
