<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">ERRO</h1>

<div class="form-group row">
		<div class="text-center col-sm-3 mb-3 mb-sm-0"></div>
		<div class="text-center col-sm-6 mb-3 mb-sm-0">
			<div class="alert alert-danger">
				<strong>Usu�rio!</strong> ${errorEntity.userEntity.name}
			</div>

		</div>
	</div>
<div class="form-group row">
		<div class="text-center col-sm-3 mb-3 mb-sm-0"></div>
		<div class="text-center col-sm-6 mb-3 mb-sm-0">
			<div class="alert alert-danger">
				<strong>Detalhe!</strong> ${errorEntity.detail}
			</div>

		</div>
	</div>
	<div class="form-group row">
		<div class="text-center col-sm-3 mb-3 mb-sm-0"></div>
		<div class="text-center col-sm-6 mb-3 mb-sm-0">
			<div class="alert alert-danger">
				<strong>Data!</strong> ${errorEntity.dateEntry}
			</div>

		</div>
	</div>
	<div class="form-group row">
		<div class="text-center col-sm-3 mb-3 mb-sm-0"></div>
		<div class="text-center col-sm-6 mb-3 mb-sm-0">
			<div class="alert alert-danger">
				<strong>Hora!</strong> ${errorEntity.hourEntry}
			</div>

		</div>
	</div>


	

</div>







<c:import url="/WEB-INF/cabecalho/footer.jsp" />
