<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />

<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">Lista de Atos
		Notarial inseridos ${textodata}</h1>


	<!-- DataTales Example -->


	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Atos Notarial</h6>
		</div>
			
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered sortable" width="100%" id="dataTable"
					cellspacing="0">
					<thead>
						<tr>
							<th>Entrada</th>
							<th>Saida</th>
							<th>Interessado</th>
							<th>Servi�o</th>
							<th>Envelope</th>
							<th>Status</th>
							<th>Op��o</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="list" items="${list}">
							<tr>
								<td>${list.dateEntry}</td>
								<td>${list.dateDeparture}</td>
								<td>${list.personName}</td>
								<td>${list.documentAtos.name}</td>
								<td>${list.envelopeEntity.name}</td>
								<td>${list.documentStatus.name}</td>
								<td><a href="<c:url value='/atos/edit/${list.id}'/>"
									class="btn btn-primary btn-sm"> Editar </a>
								<a href="<c:url value='/atos/delete/${list.id}'/>"
									class="btn btn-danger btn-sm"> Deletar </a></td>
							</tr>
						</c:forEach>


					</tbody>

				</table>

			

			</div>

		</div>
	</div>

</div>







<c:import url="/WEB-INF/cabecalho/footer.jsp" />
