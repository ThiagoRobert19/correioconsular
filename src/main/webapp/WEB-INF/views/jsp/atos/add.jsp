<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<c:import url="/WEB-INF/cabecalho/menu.jsp" />
<c:import url="/WEB-INF/cabecalho/top.jsp" />
<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">

				<div class="col-lg-12">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Nova Entrada de Atos
								Notarial</h1>
						</div>

						<form class="user" action="<c:url value='/atos/saveupdate'/>"
							method="post" enctype="multipart/form-data">
							<div class="form-group row">
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Interessado</span> <input type="text"
										class="form-control" required="required" name="personName">
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Cidade</span> <input type="text" class="form-control"
										required="required" name="personCity">
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Atendente</span> <input type="text" class="form-control"
										readonly="readonly" value="${userEntity.name}">
								</div>


							</div>
							<div class="form-group row">
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Documento</span> <select name="documentID"
										class="form-control">
										<c:forEach var="listDocuments" items="${listDocuments}">
											<option value="${listDocuments.id}">${listDocuments.name}</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Quantidade</span> <input type="number"
										class="form-control" required="required" name="quantidade">
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Envelope</span> <select name="envelopeID"
										class="form-control">
										<c:forEach var="listEnvelopes" items="${listEnvelopes}">
											<option value="${listEnvelopes.id}">${listEnvelopes.name}</option>
										</c:forEach>
									</select>
								</div>

							</div>
							<div class="form-group row">
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Status</span> <select name="statusID"
										class="form-control">
										<c:forEach var="listDocumentStatus"
											items="${listDocumentStatus}">
											<option value="${listDocumentStatus.id}">${listDocumentStatus.name}</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Tracking Number Envio</span> <input type="text"
										class="form-control" name="trackingNumberEnvio">
								</div>
								<div class="col-sm-4 mb-3 mb-sm-0">
									<span>Tracking Number Retorno</span> <input type="text"
										class="form-control" name="trackingNumberRetorno">
								</div>

							</div>
							<div class="form-group row">
								<div class="col-sm-3 mb-3 mb-sm-0">
									<span>Entrada</span> <input type="date" class="form-control"
										name="dataentrada" required="required">
								</div>
								<div class="col-sm-3 mb-3 mb-sm-0">
									<span>Saida</span> <input type="date" class="form-control"
										name="datasaida">
								</div>
								<div class="col-sm-6 mb-3 mb-sm-0">
									<span>Observação</span> <input type="text" class="form-control"
										name="observation">
								</div>

							</div>

							<div class="form-group row">
								<div class="col-sm-2">
									<button type="submit"
										class="btn btn-primary btn-user btn-block">
										<i class="fa fa-dot-circle-o"></i> Criar
									</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<c:import url="/WEB-INF/cabecalho/footer.jsp" />
