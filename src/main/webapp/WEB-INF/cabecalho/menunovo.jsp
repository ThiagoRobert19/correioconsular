<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<ul
	class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
	id="accordionSidebar">
	<a
		class="sidebar-brand d-flex align-items-center justify-content-center"
		href="<c:url value='/'/>">
		<div class="sidebar-brand-icon rotate-n-15"></div>
		<div class="sidebar-brand-text mx-3">
			<sup>Consulado Geral do Brasil em</sup> Miami
		</div>
	</a>



	<c:if test="${not empty clienteLogado.email}">

		<hr class="sidebar-divider my-0">

		<!-- Nav Item - Dashboard -->
		<!--li class="nav-item active"><a class="nav-link"
		href="<c:url value='/'/>"> <i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a></li-->
		<hr class="sidebar-divider my-0">
		<li class="nav-item active"><a class="nav-link"
			href="<c:url value='/'/>"> <i class="fas fa-fw fa-tachometer-alt"></i>
				<span>Tela Inicial</span></a></li>
		<c:if test="${clienteLogado.tipo=='Administrador'}">

			<li class="nav-item"><a class="nav-link collapsed" href="#"
				data-toggle="collapse" data-target="#pessoal" aria-expanded="true"
				aria-controls="pessoal"> <i class="fas fa-fw fa-cog"></i> <span>Pessoal</span>
			</a>
				<div id="pessoal" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<!--h6 class="collapse-header">People:</h6-->
						<a class="collapse-item" href="<c:url value='/user/acesso'/>">Acesso
							ao Sistema</a>

					</div>
				</div></li>
			<li class="nav-item"><a class="nav-link collapsed" href="#"
				data-toggle="collapse" data-target="#config" aria-expanded="true"
				aria-controls="config"> <i class="fas fa-fw fa-cog"></i> <span>Configuração</span>
			</a>
				<div id="config" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<!--h6 class="collapse-header">People:</h6-->
						<a class="collapse-item"
							href="<c:url value='/documentstatus/data'/>">Status de
							Documentos</a>
					</div>
				</div></li>
		</c:if>
		<c:if test="${clienteLogado.acessoPassaporte == 'Sim' || clienteLogado.email == 'Administrador'}">
			<li class="nav-item"><a class="nav-link collapsed" href="#"
				data-toggle="collapse" data-target="#passaporte"
				aria-expanded="true" aria-controls="passaporte"> <i
					class="fas fa-fw fa-wrench"></i> <span>Passaporte</span>
			</a>
				<div id="passaporte" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">

						<a class="collapse-item" href="<c:url value='/passaporte/add'/>">Cadastro</a>

						<a class="collapse-item" href="<c:url value='/passaporte/data'/>">Pesquisa</a>
						<a class="collapse-item"
							href="<c:url value='/relatorio/passaporte'/>">Relatório</a>

					</div>
				</div></li>
		</c:if>
		<c:if test="${clienteLogado.acessoAtos == 'Sim' || clienteLogado.email == 'Administrador'}">
			<li class="nav-item"><a class="nav-link collapsed" href="#"
				data-toggle="collapse" data-target="#atos"
				aria-expanded="true" aria-controls="atos"> <i
					class="fas fa-fw fa-wrench"></i> <span>Atos</span>
			</a>
				<div id="atos" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">

						<a class="collapse-item" href="#">Teste</a>

						

					</div>
				</div></li>
		</c:if>
	<c:if test="${clienteLogado.tipoCargo=='Local'}">

			<li class="nav-item"><a class="nav-link collapsed" href="#"
				data-toggle="collapse" data-target="#afastamento" aria-expanded="true"
				aria-controls="afastamento"> <i class="fas fa-fw fa-cog"></i> <span>Afastamentos</span>
			</a>
				<div id="afastamento" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<!--h6 class="collapse-header">People:</h6-->
						<a class="collapse-item" href="<c:url value='/afastamento/add'/>">Requerer</a>
						<a class="collapse-item" href="<c:url value='/afastamento/pendentes'/>">Pendêntes</a>
						<a class="collapse-item" href="<c:url value='/afastamento/aprovados'/>">Aprovados</a>
						<a class="collapse-item" href="<c:url value='/afastamento/negados'/>">Negados</a>
						<a class="collapse-item" href="<c:url value='/afastamento/utilizados'/>">Utilizados</a>

					</div>
				</div></li>
			
	</c:if>
	<c:if test="${clienteLogado.tipoCargo=='Quadro'}">

			<li class="nav-item"><a class="nav-link collapsed" href="#"
				data-toggle="collapse" data-target="#afastamento" aria-expanded="true"
				aria-controls="afastamento"> <i class="fas fa-fw fa-cog"></i> <span>Afastamentos</span>
			</a>
				<div id="afastamento" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<!--h6 class="collapse-header">People:</h6-->
						<a class="collapse-item" href="<c:url value='/afastamento/add'/>">Requerer</a>
						<a class="collapse-item" href="<c:url value='/afastamento/pendentes'/>">Pendêntes</a>
						<a class="collapse-item" href="<c:url value='/afastamento/aprovados'/>">Aprovados</a>
						<a class="collapse-item" href="<c:url value='/afastamento/negados'/>">Negados</a>
						<a class="collapse-item" href="<c:url value='/afastamento/utilizados'/>">Utilizados</a>

					</div>
				</div></li>
			
	</c:if>
		<hr class="sidebar-divider d-none d-md-block">
	</c:if>
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
	<div id="content">