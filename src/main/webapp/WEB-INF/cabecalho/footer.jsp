<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
</div>
<!-- End of Main Content -->

<c:if test="${not empty erro}">

	<div class="form-group row">
		<div class="text-center col-sm-3 mb-3 mb-sm-0"></div>
		<div class="text-center col-sm-6 mb-3 mb-sm-0">
			<div class="alert alert-danger">
				<strong>Erro!</strong> ${erro}
			</div>

		</div>
	</div>
</c:if>
<c:if test="${not empty success}">

	<div class="form-group row">
		<div class="text-center col-sm-3 mb-3 mb-sm-0"></div>
		<div class="text-center col-sm-6 mb-3 mb-sm-0">
			<div class="alert alert-success">
				<strong>${success}</strong>
			</div>

		</div>
	</div>
</c:if>
<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    	    if (charCode > 31 && (charCode < 48 || charCode > 57))
    	        return false;
    	    return true;
    	}
  </script>
<script>
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});
</script>
<script>
function myFunction() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("dataTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<!-- Footer -->
<footer class="sticky-footer bg-white">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright &copy;Thiago Robert Prado Souza</span>
		</div>
	</div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top"> <i
	class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
			</div>
			<div class="modal-body">Select "Logout" below if you are ready
				to end your current session.</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a class="btn btn-primary" href="login.html">Logout</a>
			</div>
		</div>
	</div>
</div>


<script src="<c:url value='/resources/vendor/jquery/jquery.min.js'/>"></script>
<script
	src="<c:url value='/resources/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>
<script
	src="<c:url value='/resources/vendor/jquery-easing/jquery.easing.min.js'/>"></script>
<script src="<c:url value='/resources/js/sb-admin-2.min.js'/>"></script>
<script src="<c:url value='/resources/vendor/chart.js/Chart.min.js'/>"></script>
<script src="<c:url value='/resources/js/demo/chart-area-demo.js'/>"></script>
<script src="<c:url value='/resources/js/demo/chart-pie-demo.js'/>"></script>
<script src="<c:url value='/resources/js/sorttable.js'/>"></script>


<script
	src="<c:url value='/resources/vendor/datatables/jquery.dataTables.min.js'/>"></script>
<script
	src="<c:url value='/resources/vendor/datatables/jquery.dataTables.js'/>"></script>
<script
	src="<c:url value='/resources/vendor/datatables/dataTables.bootstrap4.min.js'/>"></script>
<script
	src="<c:url value='/resources/vendor/datatables/dataTables.bootstrap4.min.css'/>"></script>
<script
	src="<c:url value='/resources/vendor/datatables/dataTables.bootstrap4.js'/>"></script>
<script
	src="<c:url value='/resources/vendor/datatables/dataTables.bootstrap4.css'/>"></script>


</body>

</html>
